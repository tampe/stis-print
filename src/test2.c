#include "../include/write.h"

#define N 100
#define M 50000
float128_t f[N];

int J = 0;
void write_float_1(int n, int j)
{
  uint8_t buf[M];
  flags_w_t flags = default_w_flag;
  
  flags.raw     = 1;
  flags.prec    = 30;
  flags.base    = 4;
  flags.binary  = 0;
  memset(buf,0,M);
  for (int i=0; i < n; i++)
    {
      print_float64((double)f[J], buf, flags);
    }
  
  printf("> %s\n",buf);

  memset(buf,0,M);
  for (int i=0; i < n; i++)
    {
      print_float32((float)f[J], buf, flags);
    }

  printf("> %s\n",buf);
  
  memset(buf,0,M);
  for (int i=0; i < n; i++)
    {
      print_float128(f[J], buf, flags);
    }
  
  printf("> %s\n",buf);
}


void write_float_2(int n)
{
  char buf[M];

  for (int i; i < n; i++)
    {
      //quadmath_snprintf(buf, M, "%.30Qg", f[J]);
      sprintf(buf,"%f",(double)f[J]);
    }
  printf("> %s\n",buf);
}

int main(int argc, char *argv[]) 
{
  real_init();

  f[0 ] = 10.2;
  f[1 ] = 1.03;
  f[2 ] = 0.193;
  f[3 ] = 6.6e3;
  f[4 ] = 5.5e2;
  f[5 ] = ((float128_t)1.123456789)/3;
  f[6 ] = ((float128_t) 3333333333333333)/10;
  f[7 ] = ((float128_t) 2222222222222222)/100;
  f[8 ] = ((float128_t) 1111111111111111)/1000;
  f[9 ] = ((float128_t) 2222222222222222)/10000;
  f[10] = ((float128_t) 3333333333333333)/100000;
  f[11] = ((float128_t) 4444444444444444)/1000000;
  f[12] = ((float128_t) 5555555555555555)/10000000;
  f[13] = ((float128_t) 7777777777777777)/100000000;
  f[14] = ((float128_t) 8888888888888888)/1000000000;
  f[15] = ((float128_t) 1111111111111111)*9/10000000000;

  f[5] = powq(f[5],20);
  
  if (argc == 2)
    {
      if (argv[1][0] == '1')
	write_float_1(10*1000*1000*0+1, 0);
      else
	write_float_2(10*1000*1000);
    }
}

/*
a  = y  10 + b

a1 = y1 10 + b1
a2 = y2 10 + b2

  a1 + a2 = (y1+y2)10 + b1 + b2
  a1 * a2 = (y1*y2*10 + b1y2 + b2y1)*10 + b1b2
*/

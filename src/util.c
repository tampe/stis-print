#include "../include/util.h"
int is_me_big;

int16_t rev16(int16_t x)
{
  uint16_t xx = __builtin_bswap16(*(uint16_t *)&x);
  return *(int16_t *)&xx;
}

 int32_t  rev32(int32_t x)
{
  uint32_t xx = __builtin_bswap32(*(uint32_t *)&x);
  return *(int32_t *)&xx;
}

 int64_t  rev64(int64_t x)
{
  uint64_t xx = __builtin_bswap64(*(uint64_t *)&x);
  return *(int64_t *)&xx;
}

 int128_t  rev128(int128_t x)
{
  uint128_t x0 = *(uint128_t *)&x;
  uint64_t  x1 = x0 & (~0UL);
  uint64_t  x2 = x0 >> 64;
  uint128_t x3 =
    (((uint128_t)__builtin_bswap64(x1)) << 64) |
    ((uint128_t) __builtin_bswap64(x2));

  return *(int128_t *)&x3;
}

uint16_t urev16(uint16_t x)
{
  return __builtin_bswap16(x);
}

uint32_t  urev32(uint32_t x)
{
  return __builtin_bswap32(x);
}

 uint64_t  urev64(uint64_t x)
{
  return __builtin_bswap64(x);
}

uint128_t  urev128(uint128_t x)
{
  uint64_t x1 = x & (~0UL);
  uint64_t x2 = x >> 64;
  return
    (((uint128_t)__builtin_bswap64(x1)) << 64) |
    ((uint128_t) __builtin_bswap64(x2));
}

float16_t frev16(float16_t x)
{
  uint16_t xx = __builtin_bswap16(*(uint16_t *)&x);
  return *(float16_t *)&xx;
}

float32_t  frev32(float32_t x)
{
  uint32_t xx = __builtin_bswap32(*(uint32_t *)&x);
  return *(float32_t *)&xx;
}

float64_t  frev64(float64_t x)
{
  uint64_t xx = __builtin_bswap64(*(uint64_t *)&x);
  return *(float64_t *)&xx;
}

float128_t  frev128(float128_t x)
{
  uint128_t x0 = *(uint128_t *)&x;
  uint64_t  x1 = x0 & (~0UL);
  uint64_t  x2 = x0 >> 64;
  uint128_t x3 =
    (((uint128_t)__builtin_bswap64(x1)) << 64) |
    ((uint128_t) __builtin_bswap64(x2));
  
  return *(float128_t *)&x3;
}


void MOVE(uint8_t *pt, int offset, int n, int d, int D, int N)
{
  for (int i = 0, j = n-N; i < n - offset; i+=N , j-=N)
    {
      pt[j + d + D] = pt[j + D];
    }
}

void MEMSET(uint8_t *pt, uint8_t ch, int offset, int n, int D, int N)
{
  for (int i = 0, j = offset; i < n; i+=N , j+=N)
    {
      pt[j] = ch;
    }
}


int min(int x, int y)
{
  return x < y ? x : y;
}

int max(int x, int y)
{
  return x > y ? x : y;
}


#include "../include/myinteger.h"
#include "error.c"
#include "flags.c"
#include "util.c"
int is_me_big = 0;

 uint128_t div10q(uint128_t x)
{
  uint64_t   x1  = (uint64_t)(x >> 64);
  uint128_t  x2  = (((uint128_t)1) << 64);
  uint64_t   x3  = (uint64_t)(x & (~0UL));
  
  uint64_t   b1   = x1%10;
  uint128_t  y1  = x1/10;

  uint64_t   b2  = x2%10;
  uint128_t  y2  = x2/10;
    
  uint128_t yy1 = y1*y2*10+b1*y2+b2*y1 + (b1*b2)/10;
  uint64_t  bb1 = (b1*b2)%10;

  uint64_t  bb2 = x3 % 10;
  uint128_t yy2 = x3 / 10;

  return yy1+yy2+(bb1+bb2)/10;
}

 uint128_t divE9q(uint128_t x)
{
  uint64_t   x1  = (uint64_t)(x >> 64);
  uint128_t  x2  = (((uint128_t)1) << 64);
  uint64_t   x3  = (uint64_t)(x & (~0UL));
  
  uint64_t   b1   = x1%(1000*1000*1000);
  uint128_t  y1  = x1/(1000*1000*1000);

  uint64_t   b2  = x2%(1000*1000*1000);
  uint128_t  y2  = x2/(1000*1000*1000);
    
  uint128_t yy1 = y1*y2*(1000*1000*1000)+ b1*y2 + b2*y1 +
    (b1*b2)/(1000*1000*1000);
  
  uint64_t  bb1 = (b1*b2)%(1000*1000*1000);

  uint64_t  bb2 = x3 % (1000*1000*1000);
  uint128_t yy2 = x3 / (1000*1000*1000);

  return yy1+yy2+(bb1+bb2)/(1000*1000*1000);
}

 uint64_t mod10q(uint128_t x)
{
  uint64_t  x1 = (uint64_t)(x >> 64);
  uint128_t x2 = (uint128_t)(((uint128_t)1)<<64);
  uint64_t  x3 = (uint64_t)(x & (~0UL));
  
  uint64_t  b1  = x1%10;
  uint64_t  b2  = x2%10;

  uint64_t  bb1 = (b1*b2)%10;
  uint64_t  bb2 = x3 % 10;

  return (bb1+bb2)%10;
}


 uint64_t mod100q(uint128_t x)
{
  uint64_t  x1 = (uint64_t)(x >> 64);
  uint128_t  x2 = (uint128_t)(((uint128_t)1)<<64);
  uint64_t  x3 = (uint64_t)(x & (~0UL));
  
  uint64_t  b1  = x1%100;
  uint64_t  b2  = x2%100;

  uint64_t  bb1 = (b1*b2)%100;
  uint64_t  bb2 = x3 % 100;

  return (bb1+bb2)%100;
}

 uint64_t modE9q(uint128_t x)
{
  uint64_t  x1 = (uint64_t)(x >> 64);
  uint128_t x2 = (uint128_t)(((uint128_t)1)<<64);
  uint64_t  x3 = (uint64_t)(x & (~0UL));
  
  uint64_t  b1  = x1%(1000*1000*1000);
  uint64_t  b2  = x2%(1000*1000*1000);

  uint64_t  bb1 = (b1*b2)%(1000*1000*1000);
  uint64_t  bb2 = x3 % (1000*1000*1000);

  return (bb1+bb2)%(1000*1000*1000);
}

 uint64_t modE18q(uint128_t x)
{
  return modE9q(x)+1000*1000*1000*(modE9q(divE9q(x)));
}

 uint64_t divE18q(uint128_t x)
{
  return divE9q(divE9q(x));
}

#define mkmodx(modxq, uint128_t)			\
   uint64_t modxq(uint128_t x, uint128_t m)	\
  {							\
    if (x >= (m<<2))					\
      {							\
	if (x >= (m<<3))				\
	  {						\
	    if (x >= (m*9))				\
	      return 9;					\
	    else					\
	      return 8;					\
	  }						\
	else						\
	  {						\
	    if (x >= (m*6))				\
	      {						\
		if (x >= (m*7))				\
		  return 7;				\
		else					\
		  return 6;				\
	      }						\
	    else					\
	      {						\
		if (x >= (m*5))				\
		  return 5;				\
		else					\
		  return 4;				\
	      }						\
	  }						\
      }							\
    else						\
      {							\
	if (x >= (m << 1))				\
	  {						\
	    if (x >= m*3)				\
	      return 3;					\
	    else					\
	      return 2;					\
	  }						\
	else						\
	  {						\
	    if (x >= m)					\
	      return 1;					\
	    else					\
	      return 0;					\
	  }						\
      }							\
  }

mkmodx(divxq, uint128_t)
  
 uint64_t mod10(uint64_t x)
{
  return x%10;
}

 uint64_t divx(uint64_t x, uint64_t y)
{
  return x/y;
}

 uint64_t mod100(uint64_t x)
{
  return x%100;
}

 uint64_t div10(uint64_t x)
{
  return x/10;
}

int B [64];
int B2[128];

uint64_t  A [20] = {};
uint128_t A2[39] = {};

int MM1[20*2];
int MM2[20*2];

uint64_t MU1[] = {(1UL<<8)-1UL, (1UL<<16)-1UL, (1UL<<32)-1UL,
		  ((1UL<<63)-1UL)|(1UL<<63)};

uint64_t MS1[] = {(1UL<<7)-1UL, (1UL<<15)-1UL, (1UL<<31)-1UL,
		  (1UL<<63)-1UL};
uint64_t MS2[] = {1UL<<7      , 1UL<<15      , 1UL<<31      ,
		  1UL<<63      };

uint8_t  MU [] = {4, 6, 11, 21};
uint8_t  MS [] = {4, 6, 11, 20};

uint8_t  PU [] = {0, 0, 0 , 20};
uint8_t  PS [] = {0, 0, 0 , 19};

int log10_uint128_t(uint128_t n)
{
  uint128_t power_of_10 = 1;
  int digits = 0;
  
  // Find the largest power of 10 less than the integer
  while (power_of_10 <= n / 10) {
    power_of_10 *= 10;
    digits++;
  }
  
  return digits;
}

#define JUSTIFY_AND_FILL						\
  {									\
    if (JUST)								\
      {									\
	if (n < NMAX)							\
	  {								\
	    int d = 0;							\
	    if (flags.c_justified)					\
	      {								\
		d = (((NMAX - n) / 2) / N)*N;				\
	      }								\
	    else if (flags.r_justified)					\
	      {								\
		d = NMAX - n;						\
	      }								\
	    else							\
	      {								\
		d = 0;							\
	      }								\
									\
	    int offset = 0;						\
	    if (d)							\
	      MOVE(pt, offset, n, d, D, N);				\
	    								\
	    char fill = ' ';						\
	    if (flags.fillzero)						\
	      fill = '0';						\
	    								\
	    if (d)							\
	      MEMSET(pt, fill, offset, d, D, N);			\
									\
	    MEMSET(pt, ' ' , n + d, NMAX - d - n, D, N);		\
	    								\
	    return NMAX;						\
	  }								\
      }									\
  }

#define INTPRE_ADV							\
  Sh    = flags.ch_width;						\
  D     = flags.ch1_offset;						\
  N     = 1 << Sh;							\
  									\
  n     = 0;								\
									\
  NMAX0 = flags.fieldsize*N;						\
									\
  NMAX  = 1 << flags.safe_size;						\
  NMAX  = ((NMAX0 > 0) ? min(NMAX, NMAX0) : NMAX);			\
  MM    = (flags.prec ? min(flags.prec, MM) : MM);			\
  JUST  = NMAX0;							\
  PLUS  = 0;

#define INTPRE_RAW							\
  Sh    = 0;								\
  D     = 0;								\
  N     = 1;								\
									\
  n     = 0;								\
  									\
  NMAX0 = flags.fieldsize*N;						\
  MM    = (flags.prec ? min(flags.prec, MM) : MM);			\
  NMAX  = 1 << flags.safe_size;						\
  NMAX  = ((NMAX0 > 0) ? min(NMAX, NMAX0) : NMAX);			\
  									\
  JUST  = NMAX0;							\
  PLUS  = flags.plus;


#define INTPREX(RAW)					\
  int Sh,D,N,n,NMAX0,NMAX,JUST,PLUS;			\
  if (flags.raw)					\
    {							\
      return RAW;					\
    }							

#define INTPREX0				\
  int Sh,D,N,n,NMAX0,NMAX,JUST,PLUS;		


#define INT_ZERO(x_)				\
  {						\
    if (x_ == 0)				\
      {						\
	if (n < NMAX)				\
	  {					\
	    pt[n+D] = '0';			\
	    n += N;				\
	    return n;				\
	  }					\
	else					\
	  {					\
	    return n;				\
	  }					\
      }						\
  }

#define INT_PLUS				\
  {						\
    if (PLUS)					\
      {						\
	if (n >= NMAX) return n;		\
						\
	if (flags.plus_space)			\
	  {					\
	    pt[n+D] = ' ';			\
	    n += N;				\
	  }					\
	else					\
	  {					\
	    pt[n+D] = '+';			\
	    n += N;				\
	  }					\
      }						\
  }

#define FIND(A)					\
  if (x < A[sh])				\
    {						\
      sh--;					\
      if (x < A[sh]) sh--;			\
    }						\
  						\
  for(; sh >= 0 ; sh--)				\
    {						\
      if (n >= NMAX) return n;			\
						\
      int ch = x/A[sh];				\
      pt[n + D] = '0' + ch;			\
      n += N;					\
      x -= ch*A[sh];				\
    }

#define NEG(uint64_t)				\
  uint64_t x;					\
  if (x_ < 0)					\
    {						\
      if (n >= NMAX) return n;			\
      						\
      x = (uint128_t)(-x_);			\
      pt[n+D] = '-';				\
      n += N;					\
    }						\
  else						\
    {						\
      INT_PLUS;					\
      x = x_;					\
    }

#define PRE if (!flags.compiled) flags = compile_w_flags(flags)

#define NONEG(uint64_t)				\
  uint64_t x = x_;				\
  INT_PLUS

#define INTDEFS  int sh = B[64-__builtin_clzl(x)+1]
#define BINTDEFS int sh = 64-__builtin_clzl(x);

#define MKINTW(print_int64_raw, int64_t, NEG, DEFS, INTPREX, INTPRE, BIN) \
  uint32_t print_int64_raw(int64_t x_, uint8_t *pt, flags_w_t flags) \
  {									\
    PRE;								\
    									\
    int MM = 0;								\
									\
    INTPREX;								\
									\
    BIN;								\
									\
    INTPRE;								\
    									\
    INT_ZERO(x_);							\
    									\
    NEG(int64_t);							\
    									\
    DEFS;								\
  									\
    FIND(A);								\
									\
    JUSTIFY_AND_FILL;							\
									\
    return n;								\
  }									\

#pragma GCC diagnostic push						
#pragma GCC diagnostic ignored "-Wunused-variable"			
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"


#define DEFSH								\
  int sh;								\
  if (x < ((uint128_t)1) << 65)						\
    sh = B2[64-__builtin_clzl(((uint64_t) x)) + 1];			\
  else									\
    sh = B2[64-__builtin_clzl(((uint64_t) (x >> 64))) + 1] + 64;


#define INT_PREFIX							\
  {									\
    if (flags.prefix)							\
      {									\
	if (n >= NMAX - N) return n;					\
									\
	if (flags.c)							\
	  {								\
	    static const char binpre[6] = {'b','q','o','x','y','z'};	\
	    pt[n+D] = '0'; n += N;					\
	    pt[n+D] = binpre[base]; n += N;				\
	  }								\
	else if (flags.scheme)						\
	  {								\
	    static const char binpre[6] = {'b','q','o','x','y','z'};	\
	    pt[n+D] = '#'; n += N;					\
	    pt[n+D] = binpre[base]; n += N;				\
	  }								\
      }									\
  }

#define BINPRE(uint64_t)			\
  int      base   = flags.base;			\
  int      nbits  = base + 1;			\
  uint64_t MB     = 1 << nbits;

#define FK								\
  int sh2 = ((sh  / nbits) * nbits);					\
  if (sh == sh2)							\
    sh = sh2-nbits;							\
  else									\
    sh = sh2;								\
  uint64_t mask = (MB-1) << sh;						

#define FKNUW								\
  FK;									\
  									\
  for(; sh >= 0; sh -= nbits)						\
    {									\
      if (n >= NMAX) return n;						\
      									\
      int ch = (x&mask) >> sh;						\
									\
      if (ch < 10)							\
	pt[n + D] = '0' + ch;						\
      else								\
	pt[n + D] = 'a' + (ch - 10);					\
      									\
      n    += N;							\
      									\
      mask >>= nbits;							\
    }

#define FKNUW2								\
  FK;									\
  for(; mask && x; mask >>= nbits, sh -= nbits, n += N)			\
    {									\
      if (n >= NMAX) return n;						\
									\
      uint64_t ch = (x&mask) >> sh;					\
									\
      if (ch < 10)							\
	pt[n + D] = '0' + ch;						\
      else								\
	pt[n + D] = 'a' + (ch - 10);					\
									\
      x -= (ch<<sh);							\
    }


#define MKINTB_W(print_int64_b_raw, int64_t, uint64_t,			\
		 DEFS, NEG, FKN, INTPREX, INTPRE)			\
       uint32_t print_int64_b_raw(int64_t x_, uint8_t *pt,		\
				  flags_w_t flags)			\
  {									\
    PRE;								\
    									\
    int MM = 0;								\
    									\
    INTPREX;								\
									\
    INTPRE;								\
    BINPRE(uint64_t);							\
									\
    NEG(int64_t);							\
									\
    INT_PREFIX;								\
    									\
    INT_ZERO(x_);							\
									\
    DEFS;								\
									\
    FKN;								\
    									\
    JUSTIFY_AND_FILL;							\
									\
    return n;								\
    }									


MKINTB_W(print_int64_b_raw, int64_t, uint64_t, BINTDEFS, NEG,
	 FKNUW,
	 INTPREX0, INTPRE_RAW);
 
MKINTB_W(print_int64_b, int64_t, uint64_t, BINTDEFS, NEG,
	 FKNUW,
	 INTPREX(print_int64_b_raw(x_, pt, flags)), INTPRE_ADV);


MKINTB_W(print_uint64_b_raw, uint64_t, uint64_t, BINTDEFS, NONEG,
	 FKNUW,
	 INTPREX0, INTPRE_RAW);
 
MKINTB_W(print_uint64_b, uint64_t, uint64_t, BINTDEFS, NONEG,
	 FKNUW,
	 INTPREX(print_uint64_b_raw(x_, pt, flags)), INTPRE_ADV);


MKINTB_W(print_int128_b_raw, int128_t, uint128_t, DEFSH, NEG,
	 FKNUW,
	 INTPREX0, INTPRE_RAW);
 
MKINTB_W(print_int128_b, int128_t, uint128_t, DEFSH, NEG,
	 FKNUW,
	 INTPREX(print_int128_b_raw(x_, pt, flags)), INTPRE_ADV);


MKINTB_W(print_uint128_b_raw, uint128_t, uint128_t, DEFSH, NONEG,
	 FKNUW,
	 INTPREX0, INTPRE_RAW);
 
MKINTB_W(print_uint128_b, uint128_t, uint128_t, DEFSH, NONEG,
	 FKNUW,
	 INTPREX(print_uint128_b_raw(x_, pt, flags)), INTPRE_ADV);




 
MKINTB_W(print_uint64_b_cut, uint64_t, uint64_t, BINTDEFS, NONEG,
	 FKNUW2,
	 INTPREX0, INTPRE_RAW);

MKINTB_W(print_uint128_b_cut, uint128_t, uint128_t, DEFSH, NONEG,
	 FKNUW2,
	 INTPREX0, INTPRE_RAW);


#define BIN(kind)						\
  if (flags.binary)						\
    return print_ ## kind ## _b(x_, pt, flags)

#define NOBIN

MKINTW(print_int64_raw, int64_t, NEG, INTDEFS,
       INTPREX0, INTPRE_RAW, NOBIN);

MKINTW(print_int64, int64_t, NEG, INTDEFS,
       INTPREX(print_int64_raw(x_,pt,flags)), INTPRE_ADV,
       BIN(int64));


MKINTW(print_uint64_raw, uint64_t, NONEG, INTDEFS,
       INTPREX0, INTPRE_RAW, NOBIN);

MKINTW(print_uint64    , uint64_t, NONEG, INTDEFS,
       INTPREX(print_uint64_raw(x_,pt,flags)), INTPRE_ADV,
       BIN(uint64));

MKINTW(print_int128_raw, int128_t, NEG, DEFSH,
       INTPREX0, INTPRE_RAW, NOBIN);

MKINTW(print_int128    , int128_t, NEG, DEFSH,
       INTPREX(print_int128_raw(x_,pt,flags)), INTPRE_ADV,
       BIN(int128));


MKINTW(print_uint128_raw, uint128_t, NONEG, DEFSH,
       INTPREX0, INTPRE_RAW, NOBIN);

MKINTW(print_uint128   , uint128_t, NONEG, DEFSH,
       INTPREX(print_uint128_raw(x_,pt,flags)), INTPRE_RAW,
       BIN(uint128));

#pragma GCC diagnostic pop


char complex[4] = {'r','c','q','o'};

#define MKINTVEC(print_vec, int_t, print_int_t, rev)			\
  uint32_t print_vec(int_t   *x, uint8_t *pt, flags_w_t flags)		\
  {									\
    int Sh    = flags.ch_width;						\
    int D     = flags.ch1_offset;					\
    int N     = 1 << Sh;						\
									\
    int nv   = flags.vecsize;						\
    int c    = flags.complex;						\
    int nc   = 1 << c;							\
									\
    int n = 0;								\
    int e = flags.endian == is_me_big;					\
    									\
    int chl, chr, chc;							\
    									\
    if (nv == 1)							\
      {									\
	if (flags.scheme)						\
	  {								\
	    pt[n + D]  = '#'       ; n += N;				\
	    pt[n + D]  = complex[c]; n += N;				\
									\
	    chl        = '(';						\
	    chr        = ')';						\
	    chc        = ' ';						\
	  }								\
	else								\
	  {								\
	    pt[n + D]  = complex[c]; n += N;				\
	    chl        = '[';						\
	    chr        = ']';						\
	    chc        = ',';						\
	  }								\
									\
	pt[n + D]  = chl; n += N;					\
      									\
	for (int k = 0; k < nc; k++)					\
	  {								\
	    if (k)							\
	      {								\
		pt[n + D] = chc; n += N;				\
	      }								\
									\
	    n += print_int_t(x[k], pt + n, flags);			\
	  }								\
									\
	pt[n + D] = chr; n += N;					\
      }									\
    else								\
      {									\
	if (flags.scheme)						\
	  {								\
	    pt[n + D]  = '#'       ; n += N;				\
	    pt[n + D]  = 'v'       ; n += N;				\
	    pt[n + D]  = complex[c]; n += N;				\
									\
	    chl        = '(';						\
	    chr        = ')';						\
	    chc        = ' ';						\
	  }								\
	else								\
	  {								\
	    pt[n + D]  = 'v'       ; n += N;				\
	    pt[n + D]  = complex[c]; n += N;				\
	    chl        = '[';						\
	    chr        = ']';						\
	    chc        = ',';						\
	  }								\
									\
	pt[n + D]  = chl; n += N;					\
      									\
	for (int i = 0, j = 0; j < nv; j++)				\
	  {								\
	    if (j)							\
	      {								\
		pt[n + D] = chc; n += N;				\
	      }								\
									\
	    if (nc > 1)							\
	      {								\
		pt[n + D] = chl; n += N;				\
	  								\
		for (int k = 0; k < nc; k++,i++)			\
		  {							\
		    if (k)						\
		      {							\
			pt[n + D] = chc; n += N;			\
		      }							\
		    							\
		    int_t xx = x[i];					\
		    if (e) xx = rev(xx);				\
		    n += print_int_t(xx, pt + n, flags);		\
		  }							\
									\
		pt[n + D] = chr; n += N;				\
	      }								\
	    else							\
	      {								\
	    int_t xx = x[i];						\
	    if (e) xx = rev(xx);					\
	    								\
		n += print_int_t(x[i], pt + n, flags);			\
		i++;							\
	      }								\
	  }								\
									\
	pt[n + D]  = chr; n += N;					\
      }									\
									\
    return n;								\
  }

MKINTVEC(print_int8_v  , int8_t  , print_int64        , +);
MKINTVEC(print_uint8_v , uint8_t , print_uint64       , +);

MKINTVEC(print_int16_v  , int16_t  , print_int64      , rev16);
MKINTVEC(print_uint16_v , uint16_t , print_uint64     , rev16);

MKINTVEC(print_int32_v  , int32_t  , print_int64      , rev32);
MKINTVEC(print_uint32_v , uint32_t , print_uint64     , rev32);

MKINTVEC(print_int64_v  , int64_t  , print_int64      , rev64);
MKINTVEC(print_uint64_v , uint64_t , print_uint64     , rev64);

MKINTVEC(print_int128_v , int128_t , print_int128     , rev128);
MKINTVEC(print_uint128_v, uint128_t, print_uint128    , rev128);

MKINTVEC(print_int64_b_v  , int64_t  , print_int64    , rev64);
MKINTVEC(print_uint64_b_v , uint64_t , print_uint64   , rev64);

MKINTVEC(print_int128_b_v , int128_t , print_int128_b , rev128);
MKINTVEC(print_uint128_b_v, uint128_t, print_uint128_b, rev128);


void printf_binary(uint64_t x)
{
  char buf[256];
  memset(buf,0,256);
  flags_w_t flags = default_w_flag;
  flags.base   = 3;
  flags.raw    = 1;
  flags.prefix = 0;
  print_uint64_b(x, (uint8_t *)buf, flags);
  printf("len: %.3lu - %s\n",strlen(buf),buf);  
}

uint32_t print_int(void *v, uint8_t *pt, flags_w_t flags)
{
  if (flags.complex || flags.vecsize)
    {
      if (flags.sign == 1)
	{
	  switch(flags.size)
	    {
	    case 0:
	      return print_int8_v((int8_t *)v, pt, flags);
	    case 1:
	      return print_int16_v((int16_t *)v, pt, flags);
	    case 2:
	      return print_int32_v((int32_t *)v, pt, flags);
	    case 3:
	      return print_int64_v((int64_t *)v, pt, flags);
	    case 4:
	      return print_int128_v((int128_t *)v, pt, flags);		
	    }
	}
      else
	{
	  switch(flags.size)
	    {
	    case 0:
	      return print_uint8_v((uint8_t *)v, pt, flags);
	    case 1:
	      return print_uint16_v((uint16_t *)v, pt, flags);
	    case 2:
	      return print_uint32_v((uint32_t *)v, pt, flags);
	    case 3:
	      return print_uint64_v((uint64_t *)v, pt, flags);
	    case 4:
	      return print_uint128_v((uint128_t *)v, pt, flags);
	    }
	}
    }
  else
    {
      if (flags.endian == is_me_big)
	{
	  if (flags.sign)
	    {       
	      int64_t val = 0;
	      switch (flags.size)
		{
		case 0:
		  val = *(uint8_t *)v;
		  break;
		case 1:
		  val = *(uint16_t *)v;
		  break;
		case 2:
		  val = *(uint32_t *)v;
		  break;
		case 3:
		  val = *(uint64_t *)v;
		  break;
		case 4:
		  {
		    int128_t vv = *(int128_t *)v;
		    return print_int128(vv, pt, flags);
		  }
		}
	      val = -val;
	      return print_int64(val, pt, flags); 
	    }
	  else
	    {       
	      uint64_t val = 0;
	      switch (flags.size)
		{
		case 0:
		  val = *(uint8_t *)v;
		  break;
		case 1:
		  val = *(uint16_t *)v;
		  break;
		case 2:
		  val = *(uint32_t *)v;
		  break;
		case 3:
		  val = *(uint64_t *)v;
		  break;
		case 4:
		  {
		    uint128_t vv = *(uint128_t *)v;
		    return print_uint128(vv, pt, flags);
		  }
		}
	      return print_uint64(val, pt, flags); 
	    }
	}
      else
	{
	  if (flags.sign)
	    {       
	      int64_t val = 0;
	      switch (flags.size)
		{
		case 0:
		  val = *(uint8_t *)v;
		  break;
		  
		case 1:
		  {
		    int16_t xx = *(int16_t *)v;
		    xx = rev16(xx);
		    val = xx;
		    break;
		  }
		  
		case 2:
		  {
		    int32_t xx = *(int16_t *)v;
		    xx = rev32(xx);
		    val = xx;
		    break;
		  }
		  
		case 3:
		  {
		    int64_t xx = *(int16_t *)v;
		    xx = rev64(xx);

		    val = xx;
		    break;
		  }
		  
		case 4:
		  {
		    int128_t xx = *(int128_t *)v;
		    xx = rev128(xx);
		    xx = -xx;
		    return print_int128(xx, pt, flags);
		  }
		}
	      val = -val;
	      return print_int64(val, pt, flags); 
	    }
	  else
	    {       
	      uint64_t val = 0;
	      switch (flags.size)
		{
		case 0:
		  val = *(uint8_t *)v;
		  break;
		case 1:
		  {
		    int16_t xx = *(int16_t *)v;
		    xx = rev16(xx);
		    val = *(uint16_t *)&xx;
		    break;
		  }
		case 2:
		  {
		    int32_t xx = *(int32_t *)v;
		    xx = rev32(xx);
		    val = *(uint32_t *)&xx;
		    break;
		  }
		case 3:
		  {
		    int64_t xx = *(int64_t *)v;
		    xx = rev64(xx);
		    val = *(uint64_t *)&xx;
		    break;
		  }
		case 4:
		  {
		    int128_t vv = *(int128_t *)v;
		    vv = rev128(vv);
		    uint128_t vvv = *(uint128_t *)&vv;
		    return print_uint128(vvv, pt, flags);
		  }
		}
	      return print_uint64(val, pt, flags); 
	    }
	}
    }
  return 0;
}

void init_myinteger()
{
  init_flags();
  
  uint16_t x = 1;
  uint8_t  y[2];
  *(uint16_t *)y = x;

  if (y[0])
    is_me_big = 0;
  else
    is_me_big = 1;
  
  MM1[0]    = 5;
  MM1[1*2]  = 1;
  MM1[2*2]  = 6;
  MM1[3*2]  = 1;
  MM1[4*2]  = 5;
  MM1[5*2]  = 5;
  MM1[6*2]  = 9;
  MM1[7*2]  = 0;
  MM1[8*2]  = 7;
  MM1[9*2]  = 3;
  MM1[10*2] = 7;
  MM1[11*2] = 0;
  MM1[12*2] = 4;
  MM1[13*2] = 4;
  MM1[14*2] = 7;
  MM1[15*2] = 6;
  MM1[16*2] = 4;
  MM1[17*2] = 4;
  MM1[18*2] = 8;
  MM1[19*2] = 1;

  MM2[0]    = 7;
  MM2[1*2]  = 0;
  MM2[2*2]  = 8;
  MM2[3*2]  = 5;
  MM2[4*2]  = 7;
  MM2[5*2]  = 7;
  MM2[6*2]  = 4;
  MM2[7*2]  = 5;
  MM2[8*2]  = 8;
  MM2[9*2]  = 6;
  MM2[10*2] = 3;
  MM2[11*2] = 0;
  MM2[12*2] = 2;
  MM2[13*2] = 7;
  MM2[14*2] = 3;
  MM2[15*2] = 3;
  MM2[16*2] = 2;
  MM2[17*2] = 2;
  MM2[18*2] = 9;

  MM2[0*2+1]  = 8;
  MM2[1*2+1]  = 0;
  MM2[2*2+1]  = 8;
  MM2[3*2+1]  = 5;
  MM2[4*2+1]  = 7;
  MM2[5*2+1]  = 7;
  MM2[6*2+1]  = 4;
  MM2[7*2+1]  = 5;
  MM2[8*2+1]  = 8;
  MM2[9*2+1]  = 6;
  MM2[10*2+1] = 3;
  MM2[11*2+1] = 0;
  MM2[12*2+1] = 2;
  MM2[13*2+1] = 7;
  MM2[14*2+1] = 3;
  MM2[15*2+1] = 3;
  MM2[16*2+1] = 2;
  MM2[17*2+1] = 2;
  MM2[18*2+1] = 9;

  uint64_t bits = 1;
  for (int i = 0; i < 64; i++)
    {
      uint64_t x = i < 62 ? ((bits << (i+2)) - 1) : ~0UL;
		    
      B[i] = (int)log10(x);
    }

  uint128_t bits2 = 1;
  uint128_t zero = 0;
  for (int i = 0; i < 128; i++)
    {
      uint128_t x = i < 126 ? ((bits2 << (i+2)) - 1) : ~zero;
		    
      B2[i] = (int)log10_uint128_t(x);
    }

  bits = 1;
  for(int i = 0; i < 20; i++, bits *= 10)
    {
      A[i] = bits;
    }

  
  bits2 = 1;
  for(int i = 0; i < 39; i++, bits2 *= 10)
    {
      A2[i] = bits2;
    }
}

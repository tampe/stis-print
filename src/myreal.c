#include "../include/myreal.h"
#include "myinteger.c"
#include "float16.c"

#define DB(X)
#define OFF16  15
#define OFF32  128
#define OFF64  1023
#define OFF128 16383

long double mypow[1100];
__float128  mypowq[1100];
int  MANBITS16  = 0;
int  MANBITS32  = 0;
int  MANBITS64  = 0;
int  MANBITS128 = 0;
int  DIG16;
int  DIG32;
int  DIG64;
int  DIG128;

typedef struct
{
  __float128 d;
  int exp;
} exp_t;

typedef struct
{
  double d;
  int exp;
} fexp_t;

typedef struct
{
  long double d;
  int exp;
} lexp_t;

exp_t  emap2[16383*2];
lexp_t emap[3000];
fexp_t fmap[500];
fexp_t hmap[100];

#define RUNX0(val, NX, cut)						\
  {									\
    uint64_t  valx = val % (uint64_t)A[9];				\
    int       k    = NX;						\
    if (cut)								\
      {									\
	if (valx)							\
	  {								\
	    valx = val % (uint64_t)A[4];				\
	    if (!valx)							\
	      {								\
		k = NX-4;						\
	      }								\
	  }								\
	else								\
	  {								\
	    valx = val % (uint64_t)A[12];				\
	    if (valx)							\
	      k = NX-9;							\
	    else							\
	      k = NX-12;						\
	  }								\
      }									\
    									\
    if (k < NX)								\
      {									\
	val = val / (uint64_t)A[NX-k];					\
	NX  = min(NX, k);						\
      }									\
									\
    int64_t ch;								\
    int mold = m;							\
    m += NX;								\
    for (int i = 0, j = (NX-1+mold)*N+n, first = 1;                     \
	 i < NX; i++, j-=N, val /= 10)					\
    {									\
      DB(printf("val %lu\n",val));					\
      ch = val%10;							\
      if (cut && ch == 0 && first) {m--;continue;}			\
      first = 0;							\
      pt[j + D] = '0' + ch;						\
    }									\
  }
  
#define RUNQ(val,NX)							\
  {									\
    if (NX < 19)							\
      {									\
	uint64_t val1 = (uint64_t)(modE18q(val));			\
	RUNX0(val1,NX,1);						\
      }									\
    else								\
      {									\
	uint64_t val2 = (uint64_t)(modE18q(val));			\
	uint64_t val1 = (uint64_t)(divE18q(val));			\
									\
	DB(printf("val1: %lu\nval2: %lu\nMM  : %d\n",val1,val2,NX));	\
	if (val2 == 0)							\
	  {								\
	    int NXX = NX-18;						\
	    RUNX0(val1,NXX,1);						\
	  }								\
	else								\
	  {								\
	    int NXX1 = NX-18;						\
	    RUNX0(val1,NXX1,0);						\
	    int NXX2 = 18;						\
	    RUNX0(val2,NXX2,1);						\
	  }								\
      }									\
  }

#define RUNX(val,NX)  RUNX0(val,NX,1);
  

#define PP(n10,E,print_uint64,prefix)				\
  if (exp < 0 && -exp + m <= n10)				\
    {								\
      int offset = -exp+1;					\
      mymemmove(pt + n + (offset<<Sh),pt + n, m, Sh, D);	\
      mymemset(pt + n, '0', offset, Sh, D);			\
      pt[n+N+D]='.';						\
      n += (m + offset)<<Sh;					\
    }								\
  else if (exp == 0)						\
    {								\
      mymemmove(pt + n + 2*N, pt + n + N, m - 1, Sh, D);	\
      pt[n+N+D] = '.';						\
      if (m - 1 == 0)						\
	{							\
	  pt[n+2*N+D] = '0';					\
	  m++;							\
	}							\
      n += (m + 1) << Sh;					\
    }								\
  else if (exp > 0 && m + exp < 10 && m <= exp+1)		\
    {								\
      int offset = exp - m + 1;					\
      mymemset(pt + n + (m<<Sh), '0', offset, Sh, D);		\
      n += (m + offset) << Sh;					\
      pt[n+D] = '.';						\
      n+= N;							\
      pt[n+D] = '0';						\
      n+=N;							\
    }								\
  else								\
    {								\
      mymemmove(pt + n + 2*N,pt + n + N, m-1, Sh, D);		\
      pt[n+N+D] = '.';						\
      if (m - 1 == 0)						\
	{							\
	  pt[n+2*N+D] = '0';					\
	  m++;							\
	}							\
								\
      n += (m + 1)<<Sh;						\
      pt[n + D] = E;						\
      n += N;							\
      if (exp < 0)						\
	{							\
	  pt[n + D] = '-';					\
	  n  += N;						\
	  exp = -exp;						\
	}							\
								\
      nexp = print_uint64(exp, pt+n, flags);			\
    }


#define REZERO(norm)				\
  if (norm(x) == 0)				\
    {						\
      pt[n       + D] = '0';			\
      pt[n +   N + D] = '.';			\
      pt[n + 2*N + D] = '0';			\
						\
      return 3*N;				\
    }

#define RESIGN(norm, sgn)			\
  if (norm(x) < 0)				\
    {						\
      x = sgn(x);				\
      pt[n+D] = '-'; n += N;			\
    }						\
  else						\
    {						\
      INT_PLUS;					\
    }
  
    #define L(x) ((uint64_t) ((x) % M[18]))
    //#define L(x) x

#define MKREW(print_qdouble_raw,float128_t,uint128_t,ff_t, MMDEF, RUNXQ, \
	      frexpq, emap2, NX_, NY, INTPREX, INTPRE, mod10, div10,RBIN,norm,sgn) \
  uint32_t print_qdouble_raw(float128_t x, uint8_t *pt, flags_w_t flags) \
  {									\
    PRE;								\
									\
    int NX = NX_+2;							\
    int MM = NX_+2;							\
									\
    MMDEF;								\
  									\
    INTPREX;								\
									\
    RBIN;								\
									\
    INTPRE;								\
									\
    REZERO(norm);							\
									\
    RESIGN(norm,sgn);							\
    									\
    int exp2, m = 0;							\
    frexpq(x, &exp2);							\
    exp2 += NY;								\
    int         exp   = emap2[exp2].exp;				\
    ff_t        dd    = emap2[exp2].d;					\
									\
    dd *= norm(x);							\
    uint128_t val  = (uint128_t)dd;					\
									\
    DB(printf ("val (1):\n%lu\n%lu\nexp = %d MM %d NX %d\n",		\
	       L(val),L(M[NX]),exp,MM,NX));				\
    									\
    if (val >= A[NX+1]) val /= 10;					\
    									\
    DB(printf ("val (2):\n%lu\n%lu\nexp = %d MM %d NX %d\n",		\
	       L(val),L(M[NX]),exp,MM,NX));				\
    									\
    val = div10(val);							\
    NX--;								\
    int digit = mod10(val);						\
  									\
    if (digit >= 5)							\
      val += 10;							\
									\
    val -= digit;							\
    									\
    									\
    DB(printf ("val (3):\n%lu exp %d : exp2 %d digit: %d\n",		\
	       L(val),exp, exp2, digit));				\
									\
    if (likely(val < M[NX-1]))						\
      {val*=10; exp--;}							\
									\
    if (unlikely(val < M[NX-1]))					\
      {val*=10; exp--;}							\
									\
    if (MM < NX)							\
      {									\
	val = val / A[NX-MM];						\
	NX  = MM;							\
      }									\
									\
    DB(printf ("val:\n%lu exp %d : exp2 %d digit: %d\n", L(val),	\
	       exp, exp2, digit));					\
									\
									\
    RUNXQ(val, NX);							\
    									\
    {									\
      int nexp = 0;							\
									\
      DB(printf("m = %d\n",m));						\
									\
      PP(10, 'e', print_uint64, 0);					\
									\
      n += nexp;							\
    }									\
									\
    JUSTIFY_AND_FILL;							\
									\
    return n;								\
  }

#define MDEF32					\
  uint32_t M[10];				\
  M[ 0] = 1;					\
  M[ 1] = 10;					\
  M[ 2] = 100;					\
  M[ 3] = 1000UL;				\
  M[ 4] = 1000UL*10;				\
  M[ 5] = 1000UL*100;				\
  M[ 6] = 1000UL*1000;				\
  M[ 7] = 1000UL*1000*10;			\
  M[ 8] = 1000UL*1000*100;			\
  M[ 9] = 1000UL*1000*1000;			

#define MDEF64					\
  uint64_t M[19];				\
  M[ 0] = 1;					\
  M[ 1] = 10;					\
  M[ 2] = 100;					\
  M[ 3] = 1000UL;				\
  M[ 4] = 1000UL*10;				\
  M[ 5] = 1000UL*100;				\
  M[ 6] = 1000UL*1000;				\
  M[ 7] = 1000UL*1000*10;			\
  M[ 8] = 1000UL*1000*100;			\
  M[ 9] = 1000UL*1000*1000;			\
  M[10] = 1000UL*1000*1000*10;			\
  M[11] = 1000UL*1000*1000*100;			\
  M[12] = 1000UL*1000*1000*1000;		\
  M[13] = 1000UL*1000*1000*1000*10;		\
  M[14] = 1000UL*1000*1000*1000*100;		\
  M[15] = 1000UL*1000*1000*1000*1000;		\
  M[16] = 1000UL*1000*1000*1000*1000*10;	\
  M[17] = 1000UL*1000*1000*1000*1000*100;	\
  M[18] = 1000UL*1000*1000*1000*1000*1000;

#define MDEF128					\
  uint128_t M[36];				\
  M[ 0] = 1;					\
  M[ 1] = 10;					\
  M[ 2] = 100;					\
  M[ 3] = 1000UL;				\
  M[ 4] = 1000UL*10;				\
  M[ 5] = 1000UL*100;				\
  M[ 6] = 1000UL*1000;				\
  M[ 7] = 1000UL*1000*10;			\
  M[ 8] = 1000UL*1000*100;			\
  M[ 9] = 1000UL*1000*1000;			\
  M[10] = 1000UL*1000*1000*10;			\
  M[11] = 1000UL*1000*1000*100;			\
  M[12] = 1000UL*1000*1000*1000;		\
  M[13] = 1000UL*1000*1000*1000*10;		\
  M[14] = 1000UL*1000*1000*1000*100;		\
  M[15] = 1000UL*1000*1000*1000*1000;		\
  M[16] = 1000UL*1000*1000*1000*1000*10;	\
  M[17] = 1000UL*1000*1000*1000*1000*100;	\
  M[18] = 1000UL*1000*1000*1000*1000*1000;	\
  M[19] = M[18]*10UL;				\
  M[20] = M[18]*100UL;				\
  M[21] = M[18]*1000UL;				\
  M[22] = M[18]*1000UL*10;			\
  M[23] = M[18]*1000UL*100;			\
  M[24] = M[18]*1000UL*1000;			\
  M[25] = M[18]*1000UL*1000*10;			\
  M[26] = M[18]*1000UL*1000*100;		\
  M[27] = M[18]*1000UL*1000*1000;		\
  M[28] = M[18]*1000UL*1000*1000*10;		\
  M[29] = M[18]*1000UL*1000*1000*100;		\
  M[30] = M[18]*1000UL*1000*1000*1000;		\
  M[31] = M[18]*1000UL*1000*1000*1000*10;	\
  M[32] = M[18]*1000UL*1000*1000*1000*100;	\
  M[33] = M[18]*1000UL*1000*1000*1000*1000;	\
  M[34] = M[18]*1000UL*1000*1000*1000*1000*10;	\
  M[35] = M[18]*1000UL*1000*1000*1000*1000*100;

#define DIVXXQ(x,n)					\
  {							\
    if ((n) > 0)					\
      {							\
	x = (x + 5*M[n+1])/M[n];			\
      }							\
  }

#define DIVXX(x,n) x = (x / M[n]);

#pragma GCC diagnostic push						
#pragma GCC diagnostic ignored "-Wunused-variable"			
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"


#undef DB
#define DB(X)

#define I128								\
  if ((man & (~0UL)) == 0)						\
    {									\
      k = __builtin_ctzl(man>>64)+64;					\
    }									\
  else									\
    {									\
      k = __builtin_ctzl(man);						\
    }									\

#define I64								\
  k = __builtin_ctzl(man);						\
  
#define MKREW_B(print_float128_b, print_uint128_b_cut,			\
		float128_t, uint128_t, frexpq, ldexpq,			\
		MANBITS128, INTPREX, INTPRE, PRINT_INTB, norm, sgn)	\
  int print_float128_b(float128_t x, uint8_t *pt, flags_w_t flags)	\
  {									\
    PRE;								\
									\
    int MM = 0;								\
   									\
    INTPREX;								\
									\
    BINPRE(uint128_t);							\
									\
    int MAN  = MANBITS128;						\
    int exp = 0;							\
    uint128_t man = (uint128_t)ldexpq(frexpq(x,&exp), MAN);		\
    DB(printf_binary(man));						\
    DB(printf("1: exp %d nbits = %d MAN %d\n", exp, nbits, MAN));	\
    if (norm(x) != 0 && exp != 0)					\
      {									\
	if (exp > 0)							\
	  {								\
	    int u = (exp % nbits);					\
	    if (u)							\
	      {								\
		MAN  += nbits-u;					\
	      }								\
	    else							\
	      {								\
		exp -= nbits;						\
	      }								\
	    exp /= nbits;						\
	  }								\
	else								\
	  {								\
	    exp = -exp;							\
									\
	    int u = (exp % nbits);					\
									\
	    if (u)							\
	      {								\
		MAN += u;						\
		exp += nbits;						\
	      }								\
	    else							\
	      {								\
		exp += nbits;						\
	      }								\
	    								\
	    exp /= nbits;						\
	    								\
	    exp = -exp;							\
	  }								\
      }									\
    {									\
      int kk = MAN % nbits;						\
      if (kk)								\
	{								\
	  MAN  += nbits-kk;						\
	  man <<= nbits-kk;						\
	}								\
    }									\
									\
    INTPRE;								\
    									\
    int m    = 0;							\
    int NX_  = (MAN / nbits);						\
    int NX   = NX_;							\
    MM   = NX;								\
    REZERO(norm);							\
									\
    RESIGN(norm, sgn);							\
									\
    INT_PREFIX;								\
									\
    DB(printf("3: %f, man: %lx\n", (double)x, man));			\
									\
    MM    = max(MM, 1);							\
    man >>= ((NX-MM)*nbits);						\
    DB(printf("5: man: %lx NX %d MM %d BITS %d %d\n", man,		\
	      NX_, MM, MAN, nbits));					\
									\
    flags.prefix = 0;							\
    flags.raw    = 1;							\
    int nn = PRINT_INTB;						\
    m = (nn >> Sh);							\
									\
    int nexp = 0;							\
									\
    PP(10,'Z', print_uint64_b,0);					\
  									\
    n += nexp;								\
									\
    JUSTIFY_AND_FILL;							\
									\
    return n;								\
  }


MKREW_B(print_float128_b_raw, print_uint128_b_cut,			
	float128_t, uint128_t, frexpq, ldexpq, MANBITS128,
	INTPREX0, INTPRE_RAW,
	print_uint128_b_cut(man, pt + n, flags),+,-);				

MKREW_B(print_float128_b, print_uint128_b_cut,			
	float128_t, uint128_t, frexpq, ldexpq, MANBITS128,
	INTPREX(print_float128_b_raw(x, pt, flags)), INTPRE_ADV,
	print_uint128_b_cut(man, pt + n, flags),+,-);				


MKREW_B(print_float64_b_raw, print_uint64_b_cut,			
	float64_t, uint64_t, frexp, ldexp, MANBITS64,
	INTPREX0, INTPRE_RAW,
	print_uint64_b_cut(man, pt + n, flags),+,-);				

MKREW_B(print_float64_b, print_uint64_b_cut,			
	float64_t, uint64_t, frexp, ldexp, MANBITS64,
	INTPREX(print_float64_b_raw(x, pt, flags)), INTPRE_ADV,
	print_uint64_b_cut(man, pt + n, flags),+,-);

MKREW_B(print_float32_b_raw, print_uint64_b_cut,			
	float32_t, uint32_t, frexpf, ldexpf, MANBITS32,
	INTPREX0, INTPRE_RAW,
	print_uint64_b_cut(man, pt + n, flags),+,-);				

MKREW_B(print_float32_b, print_uint64_b_cut,			
	float32_t, uint32_t, frexpf, ldexpf, MANBITS32,
	INTPREX(print_float32_b_raw(x, pt, flags)), INTPRE_ADV,
        print_uint64_b_cut(man, pt + n, flags),+,-);

MKREW_B(print_float16_b_raw, print_uint64_b_cut,			
	float16_t, uint16_t, hrexpf, ldexpf, MANBITS32,
	INTPREX0, INTPRE_RAW,
	print_uint64_b_cut(man, pt + n, flags),half2double,halfsgn);				

MKREW_B(print_float16_b, print_uint64_b_cut,			
	float16_t, uint16_t, hrexpf, ldexpf, MANBITS16,
	INTPREX(print_float16_b_raw(x, pt, flags)), INTPRE_ADV,
        print_uint64_b_cut(man, pt + n, flags), half2double, halfsgn);





#define RBIN(kind)						\
  if (flags.binary)						\
    return print_ ## kind ## _b (x, pt, flags)

#define NORBIN


MKREW(print_float128_raw, float128_t, uint128_t, float128_t, MDEF128, RUNQ,
      frexpq, emap2, DIG128, OFF128,
      INTPREX0, INTPRE_RAW, mod10q, div10q, NORBIN, +, -);

MKREW(print_float128    , float128_t, uint128_t, float128_t, MDEF128, RUNQ,
      frexpq, emap2, DIG128, OFF128,
      INTPREX(print_float128_raw(x, pt, flags)), INTPRE_ADV, mod10q, div10q,
      RBIN(float128), +, -);


MKREW(print_float64_raw, float64_t, uint64_t, long double, MDEF64, RUNX,
      frexp, emap, DIG64, OFF64, INTPREX0, INTPRE_RAW, mod10, div10,
      NORBIN, +, -);

MKREW(print_float64    , float64_t, uint64_t, long double, MDEF64, RUNX,
      frexp, emap, DIG64, OFF64,
      INTPREX(print_float64_raw(x, pt, flags)), INTPRE_ADV,  mod10, div10,
      RBIN(float64), +, -);


MKREW(print_float32_raw, float32_t, uint32_t, double, MDEF32, RUNX,
      frexpf, fmap, DIG32, OFF32, INTPREX0, INTPRE_RAW, mod10, div10,
      NORBIN, +, -);

MKREW(print_float32    , float32_t, uint32_t, double, MDEF32, RUNX,
      frexpf, fmap, DIG32, OFF32,
      INTPREX(print_float32_raw(x, pt, flags)), INTPRE_ADV, mod10, div10,
      RBIN(float32), +, -);

MKREW(print_float16_raw, float16_t, uint16_t, double, MDEF32, RUNX,
      hrexpf, fmap, DIG16, OFF16, INTPREX0, INTPRE_RAW, mod10, div10,
      NORBIN, half2double, halfsgn);

MKREW(print_float16    , float16_t, uint16_t, double, MDEF32, RUNX,
      hrexpf, fmap, DIG16, OFF16,
      INTPREX(print_float16_raw(x, pt, flags)), INTPRE_ADV, mod10, div10,
      RBIN(float16), half2double, halfsgn);

#pragma GCC diagnostic pop						

MKINTVEC(print_float16_v     , float16_t   , print_float16    , frev16);
MKINTVEC(print_float32_v     , float32_t   , print_float32    , frev32);
MKINTVEC(print_float64_v     , float64_t   , print_float64    , frev64);
MKINTVEC(print_float128_v    , float128_t  , print_float128   , frev128);

typedef union
{
  float16_t x;
  uint16_t n;
} bits16_t;

typedef union
{
  float x;
  uint32_t n;
} bits32_t;

typedef union
{
  double x;
  uint64_t n;
} bits64_t;

typedef union
{
  __float128 x;
  uint128_t n;
} bits128_t;
  
void real_init()
{
  init_myinteger();
  
  {
    bits16_t  xx0;
    bits32_t  xx1;
    bits64_t  xx2;
    bits128_t xx3;

    xx0.x     = double2half(1.5);    
    MANBITS16 = __builtin_ctz(xx0.n) + 1;
    DIG16     = (int) log10(pow(2,MANBITS16)) + 1;

    xx1.x = 1.5;
    MANBITS32 = __builtin_ctz(xx1.n) + 1;
    DIG32     = (int) log10(pow(2,MANBITS32)) + 1;

    xx2.x = 1.5;
    MANBITS64 = __builtin_ctzl(xx2.n)+1;
    DIG64     = (int) log10(pow(2,MANBITS64)) + 1;
    
    xx3.x = 1.5;
    MANBITS128 = __builtin_ctzl(xx3.n>>64) + 1 + 64;
    DIG128     = (int) log10q(powq(2,MANBITS128))+1;
  }

  for (int i = 0; i < OFF128*2; i++)
    {
      float128_t d = powq(2, i - OFF128);
      
      if (isnanq(d) || isinfq(d)) continue;
	
      int exp = (int) log10q(d);
	
      if (exp <= 0)
	{
	  exp--;
	}
      exp+=1;
      emap2[i].d   = powq(10, -exp + DIG128 + 1);
      emap2[i].exp = exp+1;    
    }

  for (int i = 0; i < OFF64*2; i++)
    {
      long double d = powl(2,i-OFF64);
      
      if (isnan(d) || isinf(d)) continue;
	
      int exp = (int) log10l(d);
	
      if (exp <= 0)
	{
	  exp -= 1;
	}
      exp+=1;
	
      emap[i].d   = powl(10,-exp+DIG64+1);
      emap[i].exp = exp;
    }

  for (int i = 0; i < 2*OFF32; i++)
    {
      double d = pow(2,i-OFF32);
      
      if (isnan(d) || isinf(d)) continue;
	
      int exp = (int) log10(d);

      if (exp < 0)
	{
	  exp--;
	}
      exp++;
	
      fmap[i].exp = exp;
      fmap[i].d   = powl(10,-exp+ DIG32 + 1);
    }

  for (int i = 0; i < 2*OFF16; i++)
    {
      double d = pow(2,i-OFF16);
      
      if (isnan(d) || isinf(d)) continue;
	
      int exp = (int) log10(d);

      if (exp < 0)
	{
	  exp--;
	}
      exp++;
	
      hmap[i].exp = exp;
      hmap[i].d   = powl(10,-exp+ DIG16 + 1);
    }
    
  for (int i = -500; i < 500; i++) mypow[i+500]  = powl(10,i);
  for (int i = -500; i < 500; i++) mypowq[i+500] = powq(10,i);
}

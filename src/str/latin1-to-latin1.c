static void
encode_latin1_chars_to_latin1_buf_display
  (uint8_t       *buf,
   size_t        *bufpt,
   uint64_t       buflen,
   const uint8_t *chars,
   size_t        *count,
   flags_w_t      flags)
{
  int size = buflen - *bufpt - 100;
  int n = (size >= *count ? *count : size);

  buf += *bufpt;
  for (int i  = 0; i < n; i++)
    buf[i] = chars[i];
  
  *count -= n;
  *bufpt += n;
}

static void
encode_latin1_chars_to_latin1_buf_n (uint8_t       *buf,
				     size_t        *bufpt,
				     uint64_t       buflen,
				     const uint8_t *chars,
				     size_t        *count,
				     int            Sh,
				     int            D)
{
  int size = buflen - *bufpt;
  int N = 1 << Sh;
  int M = size/N;
  int n = (M <= *count ? M : *count);
  buf += *bufpt;
  for (int i  = 0; i < n; i++)
    {
      buf[D] = chars[i];
      buf += N;
    }
  
  *count -= n;
  *bufpt += (n<<Sh);
}


static void
encode_latin1_chars_to_utf16_buf_display
(uint8_t       *buf,
 size_t        *bufpt,
 uint64_t       buflen,
 const uint8_t *chars,
 size_t        *count,
 flags_w_t      flags,
 int            D)
{
  encode_latin1_chars_to_latin1_buf_n (buf,bufpt,buflen,chars,count,1,D);
}

static void
encode_latin1_chars_to_utf32_buf_display
(uint8_t       *buf,
 size_t        *bufpt,
 uint64_t       buflen,
 const uint8_t *chars,
 size_t        *count,
 flags_w_t       flags,
 int             D)
{
  encode_latin1_chars_to_latin1_buf_n (buf,bufpt,buflen,chars,count,2,D);
}


MK_LATIN1(encode_latin1_chars_to_latin1_buf_write, LINE_UTF8_WRITE,
	  fill_write_latin1   , INIT(0,0));

MK_LATIN1(encode_latin1_chars_to_latin1_buf_extended, LINE_UTF8_SYM,
	  fill_extended_latin1, INIT(0,0));

MK_LATIN1(encode_latin1_chars_to_latin1_buf_r7rs, LINE_UTF8_R7RS,
	  fill_r7rs_latin1    , INIT(0,0));


MKUTF32(encode_utf32_chars_to_latin1_buf_write   , fill_write_latin1,
	INIT(0,0));

MKUTF32(encode_utf32_chars_to_latin1_buf_extended, fill_extended_latin1,
	INIT(0,0));

MKUTF32(encode_utf32_chars_to_latin1_buf_r7rs    , fill_r7rs_latin1,
	INIT(0,0));



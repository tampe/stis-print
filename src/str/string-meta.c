#define WWMK(LINE,work64_t,N8)					\
  {								\
    work64_t u   = *((work64_t *)(chars + chn));		\
    work64_t ww2;						\
    ww2.v         = LINE;					\
								\
    if (ww2.y) goto adv;					\
    							        \
    for (int k = 0; k < N8; k++)				\
      *((uint8_t *)(buf + (k<<Sh)  + bufn + D)) = u.v[k ];	\
								\
    bufn   += N8<<Sh;						\
    chn    += N8;						\
    limit2 -= N8;						\
  }

#define WWMK1(LINE,work64_t,N8)					\
  {								\
    work64_t u   = *((work64_t *)(chars + chn));		\
    work64_t ww2;						\
    ww2.v         = LINE;					\
								\
    if (ww2.y) goto adv;					\
    							        \
    for (int k = 0; k < N8; k++)				\
      *((uint8_t *)(buf + (k<<Sh)  + bufn + D)) = u.v;		\
								\
    bufn   += N8<<Sh;						\
    chn    += N8;						\
    limit2 -= N8;						\
  }

#define MK_LATIN1(encode_latin1,LINER,fill_utf8, INIT, ...)		\
  void encode_latin1							\
  (uint8_t       *buf,							\
   size_t        *bufpt,						\
   uint64_t       buflen,						\
   uint8_t       *chars,						\
   size_t        *count,						\
   flags_w_t      flags,						\
   ##__VA_ARGS__)							\
  {									\
    INIT;								\
    size_t limit     = *count;						\
    int64_t offset   = ((uint64_t) chars) % 16;				\
  									\
    DB(printf("start count = %d, bufpt = %d offset %d addr %lu\n",	\
	      *count, *bufpt, offset, (uint64_t) count));		\
  									\
    uint64_t  bufn = *bufpt;						\
    uint64_t  chn  = 0;							\
  									\
									\
    int N = (1<<Sh);							\
  									\
    int i, limit2, limit3, size, lim, p;				\
									\
  begin:								\
    size   = buflen - bufn;						\
    size   = ((size % 16) == 0 ? size : (size + 16 - (size % 16)));	\
    limit2 = limit  - chn;						\
    									\
    limit2 = (size < limit2 ? size : limit2);				\
    if (limit2 <= 0) goto exit;						\
    									\
    limit3 = limit2 - (limit2 % 16);					\
    									\
    if (offset >  0) goto adv;						\
    									\
    DB(printf("size16(%d) size %d, limit %d len %d total %d chn %d\n",	\
	      limit3, size, limit - chn, bufn - *bufpt, bufn,chn));	\
    									\
    for (i = 0; i < limit3; i += 16)					\
      {									\
	DB(printf("loop %d\n",i));					\
	work_t u  = *((work_t *)(chars + chn));				\
	work_t ww;							\
	ww.v      = LINER(w);						\
    									\
	if (ww.m[0] || ww.m[1]) goto adv;				\
									\
    									\
	DB(printf("transfer\n"));					\
	if (unlikely((Sh || (((uint64_t)(buf+bufn)) & 0x7))))		\
	  {								\
	    DB(printf("slow\n"));					\
	    for (int k = 0; k < 16; k+=1)				\
	      *((uint8_t *)(buf + k*N  + bufn+D)) = u.v[k];		\
	  }								\
	else								\
	  {								\
	     DB(printf("fast 8 byte aligned\n"));			\
	     *((uint64_t *)(buf + bufn)) = u.m[0];			\
	     *((uint64_t *)(buf + bufn + 8)) = u.m[1];			\
	  }								\
    									\
	bufn   += 16*N;							\
	chn    += 16;							\
	limit2 -= 16;							\
      }									\
    									\
    DB(printf("size8(%d)\n",size));					\
    									\
    if (limit2 < 8) goto do4;						\
    									\
    WWMK(LINER(ww),work64_t,8);						\
    									\
  do4:									\
    if (limit2 < 4) goto do2;						\
 									\
    WWMK(LINER(www),work32_t,4);					\
 									\
  do2:									\
    if (limit2 < 2) goto do1;						\
 									\
    WWMK(LINER(wwww),work16_t,2);					\
 									\
  do1:									\
    if (limit2 <= 0) goto exit;						\
 									\
    WWMK1(LINER(wwwww), work8_t, 1);					\
 									\
    goto exit;								\
 									\
  adv:									\
    p = 0;								\
    									\
    if (offset > 0)							\
      {									\
	lim = (limit2 > offset ? offset : limit2);			\
      }									\
    else								\
      {									\
	lim = (limit2 > 16    ? 16      : limit2);			\
      }									\
 									\
    DB(printf("lim = %d adv(%d) %d %d\n",lim, limit2, bufn - *bufpt, chn)); \
    for (i = 0; i < lim; i++)						\
      {									\
	uint8_t ch = chars[chn+i];					\
     									\
	bufn += fill_utf8(ch,buf+bufn,Sh,D,flags,&p);			\
      }									\
 									\
    limit2  -= i;							\
    chn     += i;							\
 									\
    DB(printf("limit2 %d i %d offset %d\n",limit2,i,offset));		\
 									\
    if (limit2 && (buflen - bufn) > 0)					\
      {									\
	if (offset > 0 ? (i != offset || p < offset) : (i != 16 || p != 16)) \
	  {								\
	    offset -= i;						\
	    goto adv;							\
	  }								\
	else								\
	  {								\
	    offset = 0;							\
	    goto begin;							\
	  }								\
      }									\
									\
  exit:									\
    *bufpt  = bufn;							\
    *count  = limit - chn;						\
									\
    DB(printf("adv end count = %d, bufpt = %d nprocessed %d\n",		\
	      *count,*bufpt,chn));					\
  }

#define WWRMK(LINE,work64_t,N8)					\
  {								\
    work64_t u   = *((work64_t *)(chars + chn));		\
    work64_t ww2;						\
    ww2.v         = LINE;					\
								\
    if (ww2.y) goto adv;					\
    							        \
    for (int k = 0; k < N8; k++)				\
      buf[k + bufn] = u.v[k];					\
								\
    bufn   += N8;						\
    chn    += N8;						\
    limit2 -= N8;						\
    incr_pos(pos,N8);						\
  }

#define WWRMK1(LINE)						\
  {								\
    work8_t u   = *((work8_t *)(chars + chn));			\
    work8_t ww2;						\
    ww2.v         = LINE;					\
								\
    if (ww2.y) goto adv;					\
    							        \
    buf[bufn] = u.v;						\
								\
    bufn   += 1;						\
    chn    += 1;						\
    limit2 -= 1;						\
    incr_pos(pos,1);						\
  }

#define MK_RLATIN1(encode_latin1, buf_t, N, LINER, fill_utf8)		\
  void encode_latin1							\
  (buf_t         *buf,							\
   size_t        *bufpt,						\
   uint64_t      buflen,						\
   uint8_t       *chars,						\
   size_t        *count,						\
   int           *e,							\
   SCM           pos,							\
   flags_w_t      flags)						\
  {									\
    size_t limit     = *count;						\
    int64_t offset   = ((uint64_t) chars) % 16;				\
  									\
    DB(printf("start count = %d, bufpt = %d offset %d addr %lu\n",	\
	      *count, *bufpt, offset, (uint64_t) count));		\
  									\
    uint64_t  bufn = *bufpt;						\
    uint64_t  chn  = 0;							\
									\
    int i, limit2, limit3, size, lim, p;				\
									\
  begin:								\
    size   = buflen - bufn;						\
    size   = ((size % 16) == 0 ? size : (size + 16 - (size % 16)));	\
    limit2 = limit  - chn;						\
    									\
    limit2 = (size < limit2 ? size : limit2);				\
    if (limit2 <= 0) goto exit;						\
    									\
    limit3 = limit2 - (limit2 % 16);					\
    									\
    if (offset >  0) goto adv;						\
    									\
    DB(printf("size16(%d) size %d, limit %d len %d total %d chn %d\n",	\
	      limit3, size, limit - chn, bufn - *bufpt, bufn,chn));	\
    									\
    for (i = 0; i < limit3; i += 16)					\
      {									\
	DB(printf("loop %d\n",i));					\
	work_t u  = *((work_t *)(chars + chn));				\
	work_t ww;							\
	ww.v      = LINER(w);						\
    									\
	if (ww.m[0] || ww.m[1]) goto adv;				\
									\
    									\
	DB(printf("transfer\n"));					\
	for (int k = 0; k < 16; k+=1)					\
	  buf[k + bufn] = u.v[k];					\
    									\
	bufn   += 16;							\
	chn    += 16;							\
	limit2 -= 16;							\
	incr_pos(pos,16);						\
      }									\
    									\
    DB(printf("size8(%d)\n",size));					\
    									\
    if (limit2 < 8) goto do4;						\
    									\
    WWRMK(LINER(ww),work64_t,8);					\
    									\
  do4:									\
    if (limit2 < 4) goto do2;						\
 									\
    WWRMK(LINER(www),work32_t,4);					\
 									\
  do2:									\
    if (limit2 < 2) goto do1;						\
 									\
    WWRMK(LINER(wwww),work16_t,2);					\
 									\
  do1:									\
    if (limit2 <= 0) goto exit;						\
 									\
    WWRMK1(LINER(wwwww));						\
 									\
    goto exit;								\
 									\
  adv:									\
    p = 0;								\
    									\
    if (offset > 0)							\
      {									\
	lim = (limit2 > offset ? offset : limit2);			\
      }									\
    else								\
      {									\
	lim = (limit2 > 16    ? 16      : limit2);			\
      }									\
 									\
    DB(printf("lim = %d adv(%d) %d %d\n",lim, limit2, bufn - *bufpt, chn)); \
    for (i = 0; i < lim; i++)						\
      {									\
	uint8_t ch = chars[chn+i];					\
     									\
	int j = i;							\
	bufn += fill_utf8(ch, buf+bufn, chars+chn, &j, flags, &p, e, pos); \
	incr_pos(pos,j-i);						\
	i = j;								\
	if (*e >= 1)							\
	  {								\
	    chn += i;							\
	    goto exit0;							\
	  }								\
      }									\
 									\
    limit2  -= i;							\
    chn     += i;							\
 									\
    DB(printf("limit2 %d i %d offset %d\n",limit2,i,offset));		\
 									\
    if (limit2 && (buflen - bufn) > 0)					\
      {									\
	if (offset > 0 ? (i != offset || p < offset) : (i != 16 || p != 16)) \
	  {								\
	    offset -= i;						\
	    goto adv;							\
	  }								\
	else								\
	  {								\
	    offset = 0;							\
	    goto begin;							\
	  }								\
      }									\
									\
  exit:									\
    *e = 3;								\
  exit0:								\
    *bufpt  = bufn;							\
    *count  = limit - chn;						\
									\
    DB(printf("adv end count = %d, bufpt = %d nprocessed %d\n",		\
	      *count,*bufpt,chn));					\
  }

#define MKUTF32(encode_utf32,fill_utf8,INIT, ...)		\
  void encode_utf32 (uint8_t        *buf,			\
		     size_t         *bufpt,			\
		     uint64_t        buflen,			\
		     const uint32_t *chars,			\
		     size_t         *count,			\
		     flags_w_t       flags,			\
		     ##__VA_ARGS__)				\
  {								\
    INIT;							\
    size_t max   = buflen - 100;				\
    size_t limit = *count;					\
    								\
    size_t n = *bufpt;						\
    int    j = 0;						\
    int    p = 0;						\
    for (size_t i = 0; i < limit && n < max; i++, j++)		\
      {								\
	uint32_t ch = chars[i];					\
								\
	n += fill_utf8(ch,buf+n,Sh,D,flags,&p);			\
      }								\
    								\
    *bufpt = n;							\
    *count = *count - j;					\
  }

#define MKRUTF32(encode_utf32, buf_t, char_t, fill_utf8,N,REV)	\
  void encode_utf32 (buf_t          *buf,			\
		     size_t         *bufpt,			\
		     uint64_t        buflen,			\
		     char_t         *chars,			\
		     size_t         *count,			\
		     int	    *e,				\
		     SCM            pos,			\
		     flags_w_t       flags)			\
  {								\
    size_t max   = buflen;					\
    size_t limit = *count;					\
								\
    size_t n = *bufpt;						\
    int    j = 0;						\
    int    p = 0;						\
    int    i = 0;							\
    for (; i < limit && n < max; i++)					\
      {									\
	uint32_t ch = REV0(chars[i]);					\
									\
	int j = i;							\
	n += fill_utf8(ch, buf + n, chars, &j, flags, &p, e, pos);	\
	incr_pos(pos,j-i);						\
	i = j;								\
	if (*e >= 1)							\
	  {								\
	    goto out;							\
	  }								\
      }									\
    									\
    *e = 3;								\
  out:									\
    *bufpt = n;								\
    *count = *count - j;						\
  }


#define LINE_UTF8(w) (u.v >= w##128 .v)

#define LINE_UTF8_WRITE(w)			\
  ( (u.v >= w##127 .v)  |			\
    (u.v <  w##32  .v ) |			\
    (u.v == w##92  .v ) |			\
    (u.v == w##39  .v ) |			\
    (u.v == w##34  .v ))			\

#define LINE_UTF8_SYM(w)						\
  (  (u.v <  w##32  .v) |						\
     ((u.v >= w##127 .v) & ((u.v < w##161 .v) | (u.v == w##173 .v))))

#define LINE_UTF8_R7RS(w)						\
  (  (u.v <  w##32  .v) |						\
    ((u.v >= w##127 .v) & ((u.v < w##161 .v) | (u.v == w##173 .v))) |	\
     (u.v == w##92  .v) |						\
     (u.v == w##124 .v))
  
#define MKFILLW(fill_write,LINE,LINE2, ...)				\
  inline int fill_write(uint32_t ch, uint8_t *buf, int Sh, int D,		\
			flags_w_t flags, int *p, ##__VA_ARGS__)		\
  {									\
    if (ch < 32)							\
      {									\
	if (ch == 10)							\
	  {								\
	    if (flags.esc_nl)						\
	      {								\
		buf[D]         = '\\';					\
		buf[(1<<Sh)+D] = 'n';					\
		return 2<<Sh;						\
	      }								\
	    else							\
	      {								\
		p++;							\
		buf[D] = ch;						\
		return 1<<Sh;						\
	      }								\
	  }								\
	else if (ch <= 13 && ch >= 7)					\
	  {								\
	    if (flags.r6rs_esc)						\
	      {								\
		hexitn(buf,ch,Sh,D);					\
		return 4<<Sh;						\
	      }								\
	    else							\
	      {								\
		char v[7] = {'a','b','t','n','v','f','r'};		\
		buf[D] = '\\';						\
		buf[(1<<Sh)+D] = v[ch-7];				\
		return 2 << Sh;						\
	      }								\
	  }								\
	else								\
	  {								\
	    hexitn(buf,ch,Sh,D);					\
	    return 4 << Sh;						\
	  }								\
      }									\
    else if (ch == 34 && !(flags.quote))				\
      {									\
	buf[D] = '\\';							\
	buf[(1<<Sh) + D] = '"';						\
	return 2 << Sh;							\
      }									\
    else if (ch == 39 && (flags.quote))					\
      {									\
	buf[D] = '\\';							\
	buf[(1<<Sh) + D] = '\'';					\
	return 2 << Sh;							\
      }									\
    else if (ch == 92)							\
      {									\
	buf[D] = buf[(1<<Sh)+D] = '\\';					\
	return 2 << Sh;							\
      }									\
    else if (ch < 256 && ch > 173)					\
      {									\
	return LINE;							\
      }									\
    else if (ch >= 127 && (ch < 161 || ch == 173))			\
      {									\
	hexitn(buf, ch, Sh, D);						\
	return 4 << Sh;							\
      }									\
    else if (ch < 256)							\
      {									\
	buf[D] = ch;							\
	(*p)++;								\
	return 1 << Sh;							\
      }									\
    else if (uc_is_general_category_withtable (ch,			\
					       UC_CATEGORY_MASK_L |	\
					       UC_CATEGORY_MASK_M |	\
					       UC_CATEGORY_MASK_N |	\
					       UC_CATEGORY_MASK_P |	\
					       UC_CATEGORY_MASK_S))	\
      {									\
	return LINE2;							\
      }									\
    else								\
      {									\
	return encode_escape_sequence (ch, buf, Sh, D, flags);		\
      }									\
									\
    return 0;								\
  }

MKFILLW(fill_write_utf8          ,
	(codepoint_to_utf8(ch,buf)),
	(codepoint_to_utf8(ch,buf)));

MKFILLW(fill_write_latin1        ,
	((*p)++,buf[0] = ch, 1),
	(encode_escape_sequence (ch, buf, Sh, D, flags)));

MKFILLW(fill_write_utf32_copy    ,
	((*p)++, buf[D] = ch, 4),
	(*((uint32_t *)buf) = ch, 4));

MKFILLW(fill_write_utf32_rev,
	((*p)++, (*((uint32_t *)buf) = rev32(ch)), 4),
	((*((uint32_t *)buf) = rev32(ch)), 4));

MKFILLW(fill_write_utf16,
	((*p)++, buf[D] = ch, 2),
	encode_utf16(ch,buf,flags,D))

#define MKFILL_U8(fill,LINE)						\
  int fill(uint8_t ch, uint8_t *buf, int Sh, int D,			\
	   flags_w_t flags, int *p)					\
  {									\
    if (ch < 128)							\
      {									\
	buf[D] = ch;							\
	(*p) += 1;							\
	return 1 << Sh;							\
      }									\
    else								\
      {									\
	return LINE;							\
      }									\
  }

MKFILL_U8(fill_utf8, codepoint_to_utf8(ch,buf));

#define MKFILLS(fill_sym,LINE1,LINE2, ...)				\
  inline int fill_sym(uint32_t ch, uint8_t *buf, int Sh, int D,		\
		      flags_w_t flags, int *p, ##__VA_ARGS__)		\
									\
  {									\
    int N = 1<<Sh;							\
    if (ch < 32)							\
      {									\
	return hexit2(ch,buf,Sh,D);					\
      }									\
    else if (ch > 173 && ch < 256)					\
    {									\
      int d = LINE1;							\
      if (d == N) (*p)++;						\
      return d;								\
    }									\
    else if (ch >= 127 && (ch < 161 || ch == 173))			\
      {									\
	return hexit2(ch, buf, Sh, D);					\
      }									\
    else if (ch < 256)							\
      {									\
	int d = LINE1;							\
	if (d == N) (*p)++;						\
	return d;							\
      }									\
    else if (uc_is_general_category_withtable (ch,			\
					       SUBSEQUENT_IDENTIFIER_MASK \
					       | UC_CATEGORY_MASK_Zs))	\
      {									\
	return LINE2;							\
      }									\
    else								\
      {									\
	return hexit2(ch, buf, Sh, D);					\
      }									\
									\
    return 0;								\
  }
     

MKFILLS(fill_extended_utf8,
	codepoint_to_utf8(ch, buf),
	codepoint_to_utf8(ch, buf));

MKFILLS(fill_extended_latin1,
	(buf[0] = ch, 1),
	(encode_escape_sequence (ch, buf, Sh, D, flags)));

MKFILLS(fill_extended_utf32_copy,
	(buf[D] = ch, 4),
	(*((uint32_t *)buf) = ch, 4));

MKFILLS(fill_extended_utf32_rev,
	(buf[D] = ch, 4),
	((*((uint32_t *)buf) = rev32(ch)), 4));

MKFILLS(fill_extended_utf16,
	(buf[D] = ch, 2),
	encode_utf16(ch,buf,flags,D))

#define MKFILLR(fill_r7rs, LINE1, LINE2,  ...)				\
  int fill_r7rs(uint32_t ch, uint8_t *buf, int Sh, int D,		\
		flags_w_t flags, int *p, ##__VA_ARGS__)			\
									\
  {									\
    int N = 1<<Sh;							\
    if (ch < 32)							\
      {									\
	if (ch >= 7 && ch <= 10)					\
	  {								\
	    char chs[] = {'a','b','t','n'};				\
	    buf[D] = '\\';						\
	    buf[N+D] = chs[ch-7];					\
	    return 2*N;							\
	  }								\
	else if (ch == 13)						\
	  {								\
	    buf[D]   = '\\';						\
	    buf[N+D] = 'r';						\
	    return 2*N;							\
	  }								\
	else								\
	  return hexit2(ch,buf,Sh,D);					\
      }									\
    else if (ch == 124)							\
      {									\
	buf[D]   = '\\';						\
	buf[N+D] = '|';							\
	return 2*N;							\
      }									\
    else if (ch == 92)							\
      return hexit2(ch,buf,Sh,D);					\
    else if (ch > 173 && ch < 256)					\
      {									\
	int d = LINE1;							\
	if (d == N) (*p)++;						\
	return d;							\
      }									\
    else if (ch >= 127 && (ch < 161 || ch == 173))			\
      {									\
	return hexit2(ch, buf, Sh, D);					\
      }									\
    else if (ch < 256)							\
      {									\
	int d = LINE1;							\
	if (d == N) (*p)++;						\
	return d;							\
      }									\
    else if (uc_is_general_category_withtable (ch,			\
					       SUBSEQUENT_IDENTIFIER_MASK \
					       | UC_CATEGORY_MASK_Zs))	\
      {									\
	return LINE2;							\
      }									\
    else								\
      {									\
	return hexit2(ch, buf, Sh, D);					\
      }									\
									\
    return 0;								\
  }

MKFILLR(fill_r7rs_utf8,
	codepoint_to_utf8(ch, buf),
	codepoint_to_utf8(ch, buf))

MKFILLR(fill_r7rs_latin1,
	(buf[0] = ch, 1),
	(encode_escape_sequence (ch, buf, Sh, D, flags)));

MKFILLR(fill_r7rs_utf32_copy,
	(buf[D] = ch , 4),
	(*((uint32_t *)buf) = ch , 4));

MKFILLR(fill_r7rs_utf32_rev,
	(buf[D] = ch, 4),
	(*((uint32_t *)buf) = rev32(ch), 4));

MKFILLR(fill_r7rs_utf16,
	(buf[D] = ch, 2),
	encode_utf16(ch,buf,flags,D));
	

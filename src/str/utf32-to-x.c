static void
encode_utf32_chars_to_latin1_buf_display
(uint8_t  *buf,
 size_t   *bufpt,
 size_t    buflen,
 uint32_t *chars,
 size_t   *count,
 flags_w_t  flags)
{
  size_t max   = buflen - 100;
  size_t limit = *count;
  
  size_t n = *bufpt;
  int    j = 0;

  for (size_t i = 0; i < limit && n < max; i++, j++)
    {
      uint32_t ch = chars[j];

      if (ch <= 0xef)
	buf[n++] = ch;
      else if (flags.convert == CONVERT_REPLACE)
	buf[n++] = '?';
      else if (flags.convert == CONVERT_ESCAPE)
        {
          n += encode_escape_sequence (ch, buf, 0, 0, flags);
        }
    }

  *bufpt = n;
  *count = j;
}

static void
encode_utf32_chars_to_utf8_buf_display
(uint8_t  *buf,
 size_t   *bufpt,
 size_t    buflen,
 uint32_t *chars,
 size_t   *count,
 flags_w_t  flags)
{
  size_t max   = buflen - 100;
  size_t limit = *count;
  
  size_t n = *bufpt;
  int    j = 0;
  for (size_t i = 0; i < limit && n < max; i++, j++)
    {
      uint32_t ch = chars[j];

      if (ch <= 0xef)
	buf[n++] = ch;
      else
        {
          n += codepoint_to_utf8 (ch, buf);
        }
    }

  *bufpt = n;
  *count = j;
}

static void
encode_utf32_chars_to_utf16_buf_display
(uint8_t  *buf,
 size_t   *bufpt,
 size_t    buflen,
 uint32_t *chars,
 size_t   *count,
 flags_w_t flags,
 int       D)
{
  size_t max   = buflen - 100;
  size_t limit = *count;
  
  size_t n = *bufpt;
  int    j = 0;
  for (size_t i = 0; i < limit && n < max; i++, j++)
    {
      uint32_t ch = chars[j];

      if (ch <= 0xff)
	{
	  buf[n+D] = ch;
	  n += 2;
	}
      else
        {
          n += encode_utf16(ch, buf + n, flags, D);
        }
    }

  *bufpt = n;
  *count = j;
}


static void
encode_utf32_chars_to_utf32_buf_display_copy
(uint8_t        *buf,
 size_t         *bufpt,
 uint64_t        buflen,
 const uint32_t *chars_,
 size_t         *count,
 flags_w_t       flags,
 int             D)
{
  uint8_t *chars = (uint8_t *)chars_;
  size_t limit = *count;
  
  size_t n    = *bufpt;
  int    i = 0;
  for (; i < limit && n < buflen; i+=1, n+=4)
    {
      buf[n+0] = chars[0];
      buf[n+1] = chars[1];
      buf[n+2] = chars[2];
      buf[n+3] = chars[3];
      chars += 4;    
    }

  *bufpt = n;
  *count = *count - i;
}

static void
encode_utf32_chars_to_utf32_buf_display_rev
(uint8_t        *buf,
 size_t         *bufpt,
 uint64_t        buflen,
 const uint32_t *chars_,
 size_t         *count,
 flags_w_t       flags,
 int             D)
{
  uint8_t *chars = (uint8_t *)chars_;
  size_t limit = *count;
  
  size_t n    = *bufpt;
  int    i = 0;
  for (; i < limit && n < buflen; i+=1, n+=4)
    {
      buf[n+0] = chars[3];
      buf[n+1] = chars[2];
      buf[n+2] = chars[1];
      buf[n+3] = chars[0];

      chars += 4;
    }

  *bufpt = n;
  *count = *count - i;
}


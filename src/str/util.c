#include <iconv.h>
#include <uniconv.h>
#include <unictype.h>

#define MKShD()							\
  {								\
    Sh      = flags.ch_width;					\
    D       = flags.ch1_offset;;				\
  }

#define MKRShD()						\

#define INITIAL_IDENTIFIER_MASK                                      \
  (UC_CATEGORY_MASK_Lu | UC_CATEGORY_MASK_Ll | UC_CATEGORY_MASK_Lt   \
   | UC_CATEGORY_MASK_Lm | UC_CATEGORY_MASK_Lo | UC_CATEGORY_MASK_Mn \
   | UC_CATEGORY_MASK_Nl | UC_CATEGORY_MASK_No | UC_CATEGORY_MASK_Pd \
   | UC_CATEGORY_MASK_Pc | UC_CATEGORY_MASK_Po | UC_CATEGORY_MASK_Sc \
   | UC_CATEGORY_MASK_Sm | UC_CATEGORY_MASK_Sk | UC_CATEGORY_MASK_So \
   | UC_CATEGORY_MASK_Co)

#define SUBSEQUENT_IDENTIFIER_MASK                                      \
  (INITIAL_IDENTIFIER_MASK                                              \
   | UC_CATEGORY_MASK_Nd | UC_CATEGORY_MASK_Mc | UC_CATEGORY_MASK_Me)

inline int put_string(const char *str, uint8_t *buf, int Sh, int D)
{
  int n = 0;
  for(; str[n]; n++)
    {
      buf[D] = str[n];
      buf += 1<<Sh;
    }
  
  return n<<Sh;
}

uint32_t utf8_to_utf32(uint32_t ch, uint8_t *chars, int *i, int *e)
{
  while (ch && (ch & 0xc0) != 0xc0)
    {
      ch  = chars[*i+1];
      *i += 1;
    }

  if (!ch)
    {
      *e = 1;
      return 0;
    }
  
  uint32_t ch2 = chars[*i+1];
  if ((ch & 0xe0) == 0xc0)
    {
      *i += 1;
      return (ch2 & 0x3f) | (ch & 0x1f) << 6;
    }
  else 
    {
      uint32_t ch3 = chars[*i+2];
      if ((ch & 0xf0) == 0xe0)
	{
	  *i += 2;
	  return (ch3 & 0x3f) | ((ch2 & 0x3f) << 6) | ((ch & 0xf) << 12);
	}
      else
	{
	  uint32_t ch4 = chars[*i+3];
	  *i += 3;
	  return (ch4 & 0x3f) | ((ch3 & 0x3f) << 6) | ((ch2 & 0x3f) << 12) |
	    ((ch & 0xf) << 18);
	}
    }  
}

uint32_t utf16_to_utf32(uint32_t ch, uint16_t *chars, int *i, int *e)
{
  if (ch < 0x800)
    {
      return ch;
    }
  else
    {
      uint32_t ch2 = chars[*i+1];
      *i += 1;
      return (ch & 0x3ff) | ((ch2 & 0x7ff) << 10) | 0x10000;
    }
}

uint32_t utf16_to_utf32_rev(uint32_t ch, uint16_t *chars, int *i, int *e)
{
  if (ch < 0x800)
    {
      return ch;
    }
  else
    {
      uint32_t ch2 = rev16(chars[*i+1]);
      *i += 1;
      return (ch & 0x3ff) | ((ch2 & 0x7ff) << 10) | 0x10000;
    }
}

size_t
codepoint_to_utf8 (uint32_t codepoint, uint8_t *utf8)
{
  size_t len;

  if (codepoint <= 0x7f)
    {
      len = 1;
      utf8[0] = codepoint;
    }
  else if (codepoint <= 0x7ffUL)
    {
      len = 2;
      utf8[0] = 0xc0 | (codepoint >> 6);
      utf8[1] = 0x80 | (codepoint & 0x3f);
    }
  else if (codepoint <= 0xffffUL)
    {
      len = 3;
      utf8[0] = 0xe0 | (codepoint >> 12);
      utf8[1] = 0x80 | ((codepoint >> 6) & 0x3f);
      utf8[2] = 0x80 | (codepoint & 0x3f);
    }
  else
    {
      len = 4;
      utf8[0] = 0xf0 | (codepoint >> 18);
      utf8[1] = 0x80 | ((codepoint >> 12) & 0x3f);
      utf8[2] = 0x80 | ((codepoint >> 6) & 0x3f);
      utf8[3] = 0x80 | (codepoint & 0x3f);
    }

  return len;
}

size_t
encode_escape_sequence (uint32_t ch, uint8_t *buf, int Sh, int D,
			flags_w_t flags)
{
  /* Represent CH using the in-string escape syntax.  */
  static const char hex[] = "0123456789abcdef";
  static const char escapes[7] = "abtnvfr";
  size_t i = 0;
  int N = 1<<Sh;
  
  buf[i+D] = '\\';
  i += N;
  if (ch >= 0x07 && ch <= 0x0D && ch != 0x0A)
    {
      /* Use special escapes for some C0 controls.  */    
      buf[i+D] = escapes[ch - 0x07];
      i += N;
    }
  else if (flags.hex_esc)
    {
      if (ch <= 0xFF)
        {
          buf[i+D]     = 'x';
          buf[i+N+D]   = hex[ch / 16];
          buf[i+2*N+D] = hex[ch % 16];
	  i += 3*N;
        }
      else if (ch <= 0xFFFF)
        {
          buf[i+D]     = 'u';
          buf[i+N+D]   = hex[(ch & 0xF000) >> 12];
          buf[i+2*N+D] = hex[(ch & 0xF00) >> 8];
          buf[i+3*N+D] = hex[(ch & 0xF0) >> 4];
          buf[i+4*N+D] = hex[(ch & 0xF)];
	  i += 5*N;
        }
      else if (ch > 0xFFFF)
        {
          buf[i+D]     = 'U';
          buf[i+N+D]   = hex[(ch & 0xF00000) >> 20];
          buf[i+2*N+D] = hex[(ch & 0xF0000) >> 16];
          buf[i+3*N+D] = hex[(ch & 0xF000) >> 12];
          buf[i+4*N+D] = hex[(ch & 0xF00) >> 8];
          buf[i+5*N+D] = hex[(ch & 0xF0) >> 4];
          buf[i+6*N+D] = hex[(ch & 0xF)];
	  i += 7*N;
        }
    }
  else
    {
      buf[i+D] = 'x';
      i += N;
      if (ch > 0xfffff) {buf[i+D] = hex[(ch >> 20) & 0xf];i+=N;}
      if (ch > 0x0ffff) {buf[i+D] = hex[(ch >> 16) & 0xf];i+=N;}
      if (ch > 0x00fff) {buf[i+D] = hex[(ch >> 12) & 0xf];i+=N;}
      if (ch > 0x000ff) {buf[i+D] = hex[(ch >>  8) & 0xf];i+=N;}
      if (ch > 0x0000f) {buf[i+D] = hex[(ch >>  4) & 0xf];i+=N;}
      buf[i+D] = hex[ch & 0xf];i += N;
      buf[i+D] = ';';i += N;
    }

  return i;
}

inline int encode_utf16
(
 uint32_t   ch,
 uint8_t   *buf,
 flags_w_t  flags,
 int        D
 )
  
{
  if (likely(ch < 0x800))
    {
      buf[D]   = ch & 0xff;
      buf[1-D] = ch >> 8;
      return 2;
    }
  else 
    { 
      int x = ch - 0x10000;
      x  = (x >> 10)    | 0xd800;
      ch = (ch & 0x3ff) | 0xdc00;

      buf[D]   = ch & 0xff;
      buf[1-D] = ch >> 8;

      buf[2+D]   = x & 0xff;
      buf[3-D]   = x >> 8;
      
      return 4;
    }
}

inline void hexit(uint8_t *buf, uint8_t ch)
{
  buf[0] = '\\';
  buf[1] = 'x';

  int ch1 = 0xf & ch;
  int ch2 = ch >> 4;

  if (ch1 < 10)
    buf[2] = '0' + ch1;
  else
    buf[2] = 'a' + ch1 - 10;

  if (ch2 < 10)
    buf[3] = '0' + ch2;
  else
    buf[3] = 'a' + ch2 - 10;  
}

inline void hexitn(uint8_t *buf, uint8_t ch, int Sh, int D)
{
  buf[0       + D] = '\\';
  buf[(1<<Sh) + D] = 'x';

  int ch1 = 0xf & ch;
  int ch2 = ch >> 4;

  if (ch1 < 10)
    buf[(2<<Sh) + D] = '0' + ch1;
  else
    buf[(2<<Sh) + D] = 'a' + ch1 - 10;

  if (ch2 < 10)
    buf[(3<<Sh) + D] = '0' + ch2;
  else
    buf[(3<<Sh) + D] = 'a' + ch2 - 10;
}

inline int hexit2(uint32_t ch, uint8_t *buf, int Sh, int D)
{
  int N = 1 << Sh;

  buf[D] = '\\';
  buf[N+D] = 'x';
  
  int i = 2*N;

  while (ch)
    {
      char chs[] = {'0','1','2','3','4','5','6','7','8','9',
		    'a','b','c','d','e','f'};
      int d = ch & 0xf;
      buf[i+D] = chs[d];
      ch >>= 4;
      i += N;
    }
  buf[i+D] = ';';

  return i + N;
}

#define INIT(a,b)				\
  int Sh = a;					\
  int D  = b;

MK_LATIN1(encode_latin1_chars_to_utf8_buf_display, LINE_UTF8,
	  fill_utf8, INIT(0,0));

MK_LATIN1(encode_latin1_chars_to_utf8_buf_write, LINE_UTF8_WRITE,
	  fill_write_utf8, INIT(0,0));

MK_LATIN1(encode_latin1_chars_to_utf8_buf_extended, LINE_UTF8_SYM,
	  fill_extended_utf8, INIT(0,0));

MK_LATIN1(encode_latin1_chars_to_utf8_buf_r7rs, LINE_UTF8_R7RS,
	  fill_r7rs_utf8, INIT(0,0));



MK_LATIN1(encode_latin1_chars_to_utf32_buf_write, LINE_UTF8_WRITE,
	  fill_write_utf32_copy, int Sh = 2, int D);

MK_LATIN1(encode_latin1_chars_to_utf32_buf_extended, LINE_UTF8_SYM,
	  fill_extended_utf32_copy, int Sh = 2, int D);

MK_LATIN1(encode_latin1_chars_to_utf32_buf_r7rs, LINE_UTF8_R7RS,
	  fill_r7rs_utf32_copy, int Sh = 2, int D);



MK_LATIN1(encode_latin1_chars_to_utf16_buf_write, LINE_UTF8_WRITE,
	  fill_write_utf16   , int Sh = 1, int D);

MK_LATIN1(encode_latin1_chars_to_utf16_buf_extended, LINE_UTF8_SYM,
	  fill_extended_utf16, int Sh = 1, int D);
	  
MK_LATIN1(encode_latin1_chars_to_utf16_buf_r7rs, LINE_UTF8_R7RS,
	  fill_r7rs_utf16    , int Sh = 1, int D);


MKUTF32(encode_utf32_chars_to_utf8_buf_write   , fill_write_utf8   , INIT(0,0));
MKUTF32(encode_utf32_chars_to_utf8_buf_extended, fill_extended_utf8, INIT(0,0));
MKUTF32(encode_utf32_chars_to_utf8_buf_r7rs    , fill_r7rs_utf8    , INIT(0,0));


MKUTF32(encode_utf32_chars_to_utf32_buf_write_copy   ,
	fill_write_utf32_copy   , int Sh = 2, int D);

MKUTF32(encode_utf32_chars_to_utf32_buf_extended_copy,
	fill_extended_utf32_copy, int Sh = 2, int D);

MKUTF32(encode_utf32_chars_to_utf32_buf_r7rs_copy    ,
	fill_r7rs_utf32_copy    , int Sh = 2, int D);



MKUTF32(encode_utf32_chars_to_utf32_buf_write_rev   ,
	fill_write_utf32_rev   , int Sh = 2, int D);

MKUTF32(encode_utf32_chars_to_utf32_buf_extended_rev,
	fill_extended_utf32_rev, int Sh = 2, int D);

MKUTF32(encode_utf32_chars_to_utf32_buf_r7rs_rev    ,
	fill_r7rs_utf32_rev    , int Sh = 2, int D);



MKUTF32(encode_utf32_chars_to_utf16_buf_write   ,
	fill_write_utf16     , int Sh = 1, int D);

MKUTF32(encode_utf32_chars_to_utf16_buf_extended,
	fill_extended_utf16  , int Sh = 1, int D);

MKUTF32(encode_utf32_chars_to_utf16_buf_r7rs    ,
	fill_r7rs_utf16      , int Sh = 1, int D);


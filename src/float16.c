float64_t half2double(float16_t x)
{
  uint16_t mantissa = (x.x & 0x3ff) | 0x400;
  uint16_t sign    = x.x & 8000;
  uint16_t exp     = (x.x >> 10) & 0x1f;

  if (exp == 0)
    {
      return NAN * (sign ? -1 : 1);
    }
  else if (exp == 31)
    {
      return INFINITY * (sign ? -1 : 1);
    }
  else
    {
      exp = exp - 15;
      if (exp == -15)
	{
	  return sign ? -(NAN) : NAN;
	}
      else if (exp == 16)
	{
	  return sign ? -(INFINITY) : INFINITY;
	}
      else
	return ldexp(mantissa, exp - 10);
    }
}

float16_t double2half(float64_t x)
{
  int exp;
  uint16_t man16 = 0;
  int sgn = (x >= 0 ? 0 : 1);
  x = (x < 0 ? -x : x);
  if (isnan(x))
    {
      exp = 0;
    }
  else if (isinf(x))
    {
      exp = 31;
    }
  else
    {
      int32_t exp2;
      double mantissa = frexp(x, &exp2);
      man16 = (uint16_t)ldexp(mantissa-1,10);
      if (exp2 <= 15 && exp2 >= -14)
	{
	  exp = exp2 + 15;
	}
      else if (exp2 > 15)
	{
	  exp = 16;
	}
      else
	{
	  exp = -15;
	}
    }

  float16_t y;
  y.x = man16 | ((exp + 15) << 10) | sgn << 15;

  return y;
}

double hrexpf(float16_t x, int *exp)
{
  uint16_t sgn = x.x & 0x8000;
  uint16_t e   = (x.x >> 10) & 0x1f;
  uint16_t m   = (x.x & 0x3ff) | 1<<10;
  
  *exp = e - 15;
  return ldexp(m, e - 10)*(sgn ? -1 : 1);
}

float16_t halfsgn(float16_t x)
{
  if (x.x & 0x8000)
    x.x = x.x & (~0x8000);
  else
    x.x = x.x | 0x8000;

  return x;
}

#undef DB
#define DB(X)

#include "../include/mystring.h"
#include "../include/mybytevector.h"
#include "mybytevector.c"
#include "str/util.c"
typedef uint8_t   v16si __attribute__ ((vector_size (16)));
typedef uint64_t  v2si  __attribute__ ((vector_size (16)));
typedef uint32_t  v4si  __attribute__ ((vector_size (16)));
typedef uint16_t  v8si  __attribute__ ((vector_size (16)));
typedef uint128_t v1si;

typedef double    w4di  __attribute__ ((vector_size (32)));

typedef union 
{
  v16si  v;
  v2si   m; 
  v4si   n;
  v8si   o;
  v1si   x;
} work_t;

typedef uint8_t   w8si __attribute__ ((vector_size (8)));
typedef union 
{
  w8si  v;
  uint64_t y;
} work64_t;

typedef uint8_t   ww4si __attribute__ ((vector_size (4)));
typedef union 
{
  ww4si  v;
  uint32_t y;
} work32_t;

typedef uint8_t   www2si __attribute__ ((vector_size (2)));
typedef union 
{
  www2si  v;
  uint16_t y;
} work16_t;

typedef union 
{
  uint8_t  v;
  uint8_t  y;
} work8_t;

work_t w0;
work_t w128;
work_t w32;
work_t w34;
work_t w127;
work_t w92;
work_t w10;
work_t w161;
work_t w173;
work_t w35;
work_t w39;
work_t w40;
work_t w41;
work_t w59;
work_t w91;
work_t w93;
work_t w123;
work_t w125;
work_t w124;

	
work64_t ww0;
work64_t ww128;
work64_t ww32;
work64_t ww34;
work64_t ww39;
work64_t ww127;
work64_t ww92;
work64_t ww10;
work64_t ww161;
work64_t ww173;
work64_t ww35;
work64_t ww40;
work64_t ww41;
work64_t ww59;
work64_t ww91;
work64_t ww93;
work64_t ww123;
work64_t ww125;
work64_t ww124;

work32_t www0;
work32_t www128;
work32_t www32;
work32_t www34;
work32_t www39;
work32_t www127;
work32_t www92;
work32_t www10;
work32_t www161;
work32_t www173;
work32_t www35;
work32_t www40;
work32_t www41;
work32_t www59;
work32_t www91;
work32_t www93;
work32_t www123;
work32_t www125;
work32_t www124;

work16_t wwww0;
work16_t wwww128;
work16_t wwww32;
work16_t wwww34;
work16_t wwww39;
work16_t wwww127;
work16_t wwww92;
work16_t wwww10;
work16_t wwww161;
work16_t wwww173;
work16_t wwww35;
work16_t wwww40;
work16_t wwww41;
work16_t wwww59;
work16_t wwww91;
work16_t wwww93;
work16_t wwww123;
work16_t wwww125;
work16_t wwww124;

work8_t wwwww0;
work8_t wwwww128;
work8_t wwwww32;
work8_t wwwww34;
work8_t wwwww39;
work8_t wwwww127;
work8_t wwwww92;
work8_t wwwww10;
work8_t wwwww161;
work8_t wwwww173;
work8_t wwwww35;
work8_t wwwww40;
work8_t wwwww41;
work8_t wwwww59;
work8_t wwwww91;
work8_t wwwww93;
work8_t wwwww123;
work8_t wwwww125;
work8_t wwwww124;

char quote_table1[256];
char quote_table2[256];

#define CHARS(str) SCM_CELL_OBJECT_LOC (str,2)



#include "str/string-meta.c"
#include "str/latin1-to-utf8.c"
#include "str/latin1-to-latin1.c"
#include "str/utf32-to-x.c"


#define MK_DISPATCH(port_encode_dispatch, write)			\
  static int port_encode_dispatch##write				\
  (									\
   void           *str,							\
   size_t         *slen,						\
   void           *buf,							\
   flags_w_t       flags)						\
    									\
  {									\
    size_t buflen  = 1000;						\
    size_t      q  = 1 << flags.size;					\
    size_t      n  = 0;							\
									\
    int Sh         = flags.ch_width;					\
    int D          = flags.ch1_offset;					\
									\
    if (q == 1)								\
      {									\
	uint8_t *chars = str;						\
									\
	if (Sh == 0)							\
	  {								\
	    if (flags.utf8)						\
	      {								\
		encode_latin1_chars_to_utf8_buf##write			\
		  (buf, &n, buflen, chars, slen, flags);		\
	      }								\
	    else							\
	      {								\
		encode_latin1_chars_to_latin1_buf##write		\
		  (buf, &n, buflen, chars, slen,flags);			\
	      }								\
	  }								\
	else if (Sh == 1)						\
	  {								\
	    encode_latin1_chars_to_utf16_buf##write			\
	      (buf, &n, buflen, chars, slen, flags, D);			\
	  }								\
	else								\
	  {								\
	    encode_latin1_chars_to_utf32_buf##write			\
	      (buf, &n, buflen, chars, slen, flags, D);			\
	  }								\
      }									\
    else								\
      {									\
	uint32_t *chars = (uint32_t *) str;				\
									\
	if (Sh == 0)							\
	  {								\
	    if (flags.utf8)						\
	      {								\
		encode_utf32_chars_to_utf8_buf##write			\
		  (buf, &n, buflen, chars, slen, flags);		\
	      }								\
	    else							\
	      {								\
		encode_utf32_chars_to_latin1_buf##write			\
		  (buf, &n, buflen, chars, slen,flags);			\
	      }								\
	  }								\
	else if (Sh == 1)						\
	  {								\
	    encode_utf32_chars_to_utf16_buf##write			\
	      (buf, &n, buflen, chars, slen, flags, D);			\
	  }								\
	else								\
	  {								\
	    if (is_me_big)						\
	      encode_utf32_chars_to_utf32_buf##write##_copy		\
		(buf, &n, buflen, chars, slen, flags, D);		\
	    else							\
	      encode_utf32_chars_to_utf32_buf##write##_rev		\
		(buf, &n, buflen, chars, slen, flags, D);		\
	  }								\
      }									\
									\
	  return n;							\
      }

MK_DISPATCH (port_encode_dispatch, _write   );
MK_DISPATCH (port_encode_dispatch, _display );
MK_DISPATCH (port_encode_dispatch, _extended);
MK_DISPATCH (port_encode_dispatch, _r7rs    );

int print_string(void *str, size_t *len, void *_buf, flags_w_t flags, int redo)
{
  int      Sh    = flags.ch_width;					
  int      D     = flags.ch1_offset;					
  int      N     = 1<<Sh;
  uint8_t *buf   = (uint8_t *)_buf;
  int n = 0;
  char qch = ((flags.quote) ? '\'' : '"');

  if (flags.write)
    {
      if (!redo)
	{
	  buf[D] = qch; n += N;
	}

      int nn;
      if (flags.r7rs)
	nn = port_encode_dispatch_r7rs(str, len, buf + n, flags);
      else if (flags.extended)
	nn = port_encode_dispatch_extended(str, len, buf + n, flags);
      else
	nn = port_encode_dispatch_write(str, len, buf + n, flags);
      
      if (nn >= 0)
	{
	  n += nn;
	  buf[n+D] = qch; n += N;
	}
      else
	{
	  n = -1;
	  buf[D] = 0;
	}
    }
  else
    {
      n += port_encode_dispatch_display(str, len, buf, flags);	      
    }

  return n;
}


void string_init()
{
  /*
  pthread_mutex_init(&iconv_lock, NULL) ;
  
  sym_replace    = scm_from_latin1_symbol ("replace"   );
  sym_escape     = scm_from_latin1_symbol ("escape"    );
  sym_substitute = scm_from_latin1_symbol ("substitute");

  sym_UTF_8      = scm_from_latin1_symbol ("UTF-8"     );
  sym_ISO_8859_1 = scm_from_latin1_symbol ("ISO-8859-1");
  sym_UTF_16     = scm_from_latin1_symbol ("UTF-16"    );
  sym_UTF_32     = scm_from_latin1_symbol ("UTF-32"    );
  sym_UTF_16BE   = scm_from_latin1_symbol ("UTF-16BE"  );
  sym_UTF_32BE   = scm_from_latin1_symbol ("UTF-32BE"  );
  sym_UTF_16LE   = scm_from_latin1_symbol ("UTF-16LE"  );
  sym_UTF_32LE   = scm_from_latin1_symbol ("UTF-32LE"  );
  */
  
  for (int i = 0; i < 16; i++)
    {
      w0.  v[i] = 0;
      w128.v[i] = 128;
      w32. v[i] = 32;
      w34. v[i] = 34;
      w39. v[i] = 39;
      w127.v[i] = 127;
      w92. v[i] = 92;
      w10. v[i] = 10;
      w161.v[i] = 161;
      w173.v[i] = 173;
      w35 .v[i] = 35;
      w40 .v[i] = 40;
      w41 .v[i] = 41;
      w59 .v[i] = 59;
      w91 .v[i] = 91;
      w93 .v[i] = 93;
      w123.v[i] = 123;
      w125.v[i] = 125;
      w124.v[i] = 124;
      
      if (i < 8)
	{
	  ww0.  v[i] = 0;
	  ww128.v[i] = 128;
	  ww32. v[i] = 32;
	  ww34. v[i] = 34;
	  ww39. v[i] = 39;
	  ww127.v[i] = 127;
	  ww92. v[i] = 92;
	  ww10. v[i] = 10;
	  ww161.v[i] = 161;
	  ww173.v[i] = 173;
	  ww35 .v[i] = 35;
	  ww40 .v[i] = 40;
	  ww41 .v[i] = 41;
	  ww59 .v[i] = 59;
	  ww91 .v[i] = 91;
	  ww93 .v[i] = 93;
	  ww123.v[i] = 123;
	  ww125.v[i] = 125;
	  ww124.v[i] = 124;
	  
	}

      if (i < 4)
	{
	  www0.  v[i] = 0;
	  www128.v[i] = 128;
	  www32. v[i] = 32;
	  www34. v[i] = 34;
	  www39. v[i] = 39;
	  www127.v[i] = 127;
	  www92. v[i] = 92;
	  www10. v[i] = 10;
	  www161.v[i] = 161;
	  www173.v[i] = 173;
	  www35 .v[i] = 35;
	  www40 .v[i] = 40;
	  www41 .v[i] = 41;
	  www59 .v[i] = 59;
	  www91 .v[i] = 91;
	  www93 .v[i] = 93;
	  www123.v[i] = 123;
	  www125.v[i] = 125;
	  www124.v[i] = 124;
	}

      if (i < 2)
	{
	  wwww0.  v[i] = 0;
	  wwww128.v[i] = 128;
	  wwww32. v[i] = 32;
	  wwww34. v[i] = 34;
	  wwww39. v[i] = 39;
	  wwww127.v[i] = 127;
	  wwww92. v[i] = 92;
	  wwww10. v[i] = 10;
	  wwww161.v[i] = 161;
	  wwww173.v[i] = 173;
	  wwww35 .v[i] = 35;
	  wwww40 .v[i] = 40;
	  wwww41 .v[i] = 41;
	  wwww59 .v[i] = 59;
	  wwww91 .v[i] = 91;
	  wwww93 .v[i] = 93;
	  wwww123.v[i] = 123;
	  wwww125.v[i] = 125;
	  wwww124.v[i] = 124;
	}

      if (i < 1)
	{
	  wwwww0.  v = 0;
	  wwwww128.v = 128;
	  wwwww32. v = 32;
	  wwwww34. v = 34;
	  wwwww39. v = 39;
	  wwwww127.v = 127;
	  wwwww92. v = 92;
	  wwwww10. v = 10;
	  wwwww161.v = 161;
	  wwwww173.v = 173;
	  wwwww35 .v = 35;
	  wwwww40 .v = 40;
	  wwwww41 .v = 41;
	  wwwww59 .v = 59;
	  wwwww91 .v = 91;
	  wwwww93 .v = 93;
	  wwwww123.v = 123;
	  wwwww125.v = 125;
	  wwwww124.v = 124;
	}
    }

  //TODO: Take this from guile environment (* in ice-9/write)
  /* SCM_R6RS_ESCAPES_P = SCM_BOOL_F; */

  for (int i = 0; i < 256; i++)
    {
      if (i <= 32)
	{
	  quote_table1[i] = 1;
	  quote_table2[i] = 1;
	}
      else if (i == 40 || i == 41 || i == 91 || i == 93 || i == 123 || i == 125)
	{
	  quote_table1[i] = 1;
	  quote_table2[i] = 1;
	}
      else if (i >= 127 && i <= 160)
	{
	  quote_table1[i] = 1;
	  quote_table2[i] = 1;
	}
      else if (i == 171 || i == 173 || i == 187)
	{
	  quote_table1[i] = 1;
	  quote_table2[i] = 1;
	}
      else
	{
	  quote_table1[i] = 0;
	  quote_table2[i] = 0;
	}
    }

  /*
  case '\'':
    case '`':
    case ',':
    case '"':
    case ';':
    case '#':
  */
  
  quote_table1['\''] = 1;
  
  quote_table1['`'] = 1;
  
  quote_table1[','] = 1;
  
  quote_table1['"'] = 1;
  quote_table2['"'] = 1;
  
  quote_table1[';'] = 1;
  quote_table2[';'] = 1;
  
  quote_table1['#'] = 1;
  quote_table2['#'] = 1;

  quote_table1['|'] = 2;
  quote_table2['|'] = 2;

  quote_table1['\\'] = 2;
  quote_table2['\\'] = 2;

  quote_table1[':'] = 3;
  
  quote_table1['.'] = 4;

  quote_table1['+'] = 5;
  quote_table1['-'] = 5;
  for (int i = 0; i < 10; i++)
    quote_table1['0' + i] = 5;
}

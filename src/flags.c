#include "endian.h"

flags_w_t default_w_flag;

#define F(x) ((uint64_t) (x))

typedef union flags_mask
{
  uint64_t  val;
  flags_w_t flags;
} mask_t;

mask_t  int_mask;

void init_flags()
{
  flags_w_t f  = {};

  f.c          = 1;
  f.prefix     = 1;
  f.sep_comma  = 1;
  f.vecsize    = 1;
  f.safe_size  = 7;
  f.sign       = 1;
  f.size       = 3;
  
  default_w_flag  = f;

  mask_t mask;
  mask.val = 0UL;
  
  mask.flags.ch_width  = (uint8_t)0x3;
  mask.flags.fieldsize = (uint8_t)0xff;
  mask.flags.plus      = 1;
  mask.flags.prefix    = 1;

  int_mask = mask;
}

flags_w_t compile_w_flags(flags_w_t f)
{
  f.compiled = 1;

  mask_t m;  
  m.val   = 0UL;
  m.flags = f;
  
  if (int_mask.val & m.val)
    {
      f.raw = 0;
      if (f.endian == 0)
	f.endian = is_me_big + 1;
  
      if (f.endian == MY_BIG_ENDIAN)
	f.ch1_offset = (1 << f.ch_width) - 1;  
      else
	f.ch1_offset = 0;
    }
  else
    {
      f.raw = 1;
    }
  
  return f;
}

flags_w_t calculate_w_size(flags_w_t f, int buflen, int maxlen)
{
  int diff = maxlen - buflen;
  
  int sh = 0;
  for (int i = 0; i <= 16 && diff >= (1 << i); i++)
    {
      sh ++;
    }
  
  f.safe_size = sh;
  
  return f;
  
}

flags_w_t calculate_w_intsize(flags_w_t f)
{
  
  if (f.binary)
    switch (f.base)      
      {
      case 0:
	f.base      = 1;

      case 1:
	f.safe_size = 8;
	break;

      case 2:
	f.safe_size = 7;
	break;

      default:
	f.safe_size = 5;
	break;
      }

  return f;
}

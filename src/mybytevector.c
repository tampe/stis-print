#include "../include/mybytevector.h"
#include "myreal.c"

#define BRUN(fkn,tp,type,p)					\
  {								\
   for (int i = 0; i < den; i++)				\
     {								\
      if (n >= len)						\
	{							\
	 *dlen = den - i;					\
	 return n;						\
	}							\
      								\
      n += fkn(tp (p(((type *) data) + i)), pt+n, flags);	\
      								\
      if (i < den-1)						\
	{							\
	  pt[n+D] = sep; n += N;				\
	}							\
     }								\
  }

#define BVRUNV(tp, sz, k)					\
  BRUN(print_ ## tp ## sz ## _v , (tp ## sz ## _t *), tp ## sz ## _t ,   )


#define BVRUN(tp, sz, k)					\
  BRUN(print_ ## tp ## k , (tp ## k ## _t) , tp ## sz ## _t, *)


#define BFRUN(BVRUN)				\
  switch (flags.size)				\
    {						\
    case 1:					\
      BVRUN(float,16, 16);			\
      break;					\
    case 2:					\
      BVRUN(float,32, 32);			\
      break;					\
    case 3:					\
      BVRUN(float,64, 64);			\
      break;					\
    case 4:					\
      BVRUN(float,128, 128);			\
    }

#define BIRUN(BVRUNV, int)			\
  switch (flags.size)				\
    {						\
    case 0:					\
      BVRUNV(int, 8, 64);			\
      break;					\
    case 1:					\
      BVRUNV(int, 16, 64);			\
      break;					\
    case 2:					\
      BVRUNV(int, 32, 64);			\
      break;					\
    case 3:					\
      BVRUNV(int, 64, 64);			\
      break;					\
    case 4:					\
      BVRUNV(int, 128, 128);			\
    }

#define RUNNER(BVRUNV)				\
  if (flags.real)				\
    {						\
      BFRUN(BVRUNV);				\
    }						\
  else if (flags.sign)				\
    {						\
      BIRUN(BVRUNV, int);			\
    }						\
  else						\
    {						\
      BIRUN(BVRUNV, uint);			\
    }


size_t print_bytevector(void *data, size_t *dlen, uint8_t *pt, size_t len,
			flags_w_t flags, int redo)
{
  size_t den = *dlen;

  int Sh    = flags.ch_width;						
  int D     = flags.ch1_offset;					
  int N     = 1 << Sh;						
  int n     = 0;
  
  int sep,left,right;							

  if (flags.scheme)
    {
      left  = '(';
      right = ')';
      sep   = ' ';

      if (!redo)
	{
	  pt[n+D] = '#'; n += N;
	  if (flags.real)
	    {
	      pt[n+D] = 'f'; n += N;
	    }
	  else if (flags.sign)
	    {
	      pt[n+D] = 's'; n += N;
	    }
	  else
	    {
	      pt[n+D] = 'u'; n += N;
	    }

	  switch (flags.size)
	    {
	    case 0:
	      pt[n+D] = '8'; n += N;
	      break;
	    case 1:
	      pt[n+D] = '1'; n += N;
	      pt[n+D] = '6'; n += N;
	      break;
	    case 2:
	      pt[n+D] = '3'; n += N;
	      pt[n+D] = '2'; n += N;
	      break;
	    case 3:
	      pt[n+D] = '6'; n += N;
	      pt[n+D] = '4'; n += N;
	      break;
	    case 4:
	      pt[n+D] = '1'; n += N;
	      pt[n+D] = '2'; n += N;
	      pt[n+D] = '8'; n += N;
	    }

	  if (flags.vecsize > 0)
	    {
	      switch (flags.complex)
		{
		case 0:
		  pt[n+D] = 'R'; n += N;
		  break;
		case 1:
		  pt[n+D] = 'C'; n += N;
		  break;
		case 2:
		  pt[n+D] = 'Q'; n += N;
		  break;
		case 3:
		  pt[n+D] = 'O'; n += N;
		}

	      if (flags.vecsize > 1)
		{
		  if (flags.vecsize >= 10)
		    {
		      pt[n+D] = '0' + (flags.vecsize/10); n += N;
		    }
		  
		  pt[n+D] = '0' + (flags.vecsize % 10); n += N;
		}
	    }
	}
    }
  else if (flags.python)
    {
      left  = '[';
      right = ']';
      sep   = ','; 	
    }
  else if (flags.c)
    {
      left  = '{';
      right = '}';
      sep   = ','; 	
    }
  else
    {
      if (flags.sep_comma)
	sep = ',';
      else
	sep = ' ';

      if (flags.paren)
	{
	  left  = '(';
	  right = ')';
	}
      else if (flags.brace)
	{
	  left  = '{';
	  right = '}';
	}
      else
	{
	  left  = '[';
	  right = ']';
	}
    }

  if (!redo)
    {
      pt[n+D] = left; n += N;
    }
  
  if (flags.vecsize > 0)						
    {
      RUNNER(BVRUNV);
    }
  else
    {
      RUNNER(BVRUN);
    }

  pt[n+D] = right; n += N;

  *dlen = 0;
  return n;
}


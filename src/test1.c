#include "../include/myinteger.h"

#define N 1000000
#define M 50000
double f[N];

#define INT (100L)
int64_t c[4];

void test_int_1(int n)
{
  uint8_t buf[M];
  memset(buf, 0, M);
  
  flags_w_t flags = default_w_flag;
  
  /*
    flags.binary = 1;
    flags.base   = (uint8_t)3;
  */
  
  
  flags.raw     = 1;
  flags.vecsize = 4;
  flags.complex = 0;
  flags.scheme  = 1;
  for (int i = 0; i < n; i++)
    {
      print_int64_v(c, buf, 1000, flags);
    }      
  
  printf(" val(1): '%s'\n", (char *)buf);
}

void test_int_2(int n)
{
  char buf[M];
  memset(buf, 0, M);

  for (int i = 0; i < n; i++)
    {
      sprintf(buf, "%lu",  INT);
    }      

  printf(" val(2): %s\n", (char *)buf);
}


int main(int argc, char *argv[]) 
{
  init_myinteger();

  c[0] = 100;
  c[1] = 101;
  c[2] = 102;
  c[3] = 103;
  
  if (argc == 2)
    {
      if (argv[1][0] == '1')
	test_int_1(10*1000*1000);
      else
	test_int_2(10*1000*1000);
    }
}

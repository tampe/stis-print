#ifndef ENDIAN_H
#define ENDIAN_H

extern int is_me_big;

#define MY_BIG_ENDIAN    2
#define MY_LITTLE_ENDIAN 1
#define MY_NATIVE_ENDIAN    0

#endif

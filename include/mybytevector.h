#ifndef MYBV_H
#define MYBV_H
#include "myreal.h"

size_t print_bytevector(void *data, size_t *dlen, uint8_t *pt, size_t len,
			flags_w_t flags, int redo);


#endif

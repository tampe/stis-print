#ifndef MYSTRING_H
#define MYSTRING_H

#include "error.h"
#include "flags.h"
#include "util.h"


int  print_string(void *str, size_t *len, void *buf, flags_w_t flags, int redo);
void string_init();

#endif

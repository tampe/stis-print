#ifndef MYREAL_H
#define MYREAL_H

#include "myinteger.h"


void real_init();

uint32_t print_float128   (float128_t x, uint8_t *pt, flags_w_t flags);
uint32_t print_float64    (float64_t  x, uint8_t *pt, flags_w_t flags);
uint32_t print_float32    (float32_t  x, uint8_t *pt, flags_w_t flags);
uint32_t print_float16    (float16_t  x, uint8_t *pt, flags_w_t flags);

uint32_t print_float128_v   (float128_t *x, uint8_t *pt, flags_w_t flags);
uint32_t print_float64_v    (float64_t  *x, uint8_t *pt, flags_w_t flags);
uint32_t print_float32_v    (float32_t  *x, uint8_t *pt, flags_w_t flags);
uint32_t print_float16_v    (float16_t  *x, uint8_t *pt, flags_w_t flags);

float64_t half2double(float16_t x);
float16_t double2half(float64_t x);
#endif

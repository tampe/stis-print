#ifndef MYINTEGER_H
#define MYINTEGER_H

#include "error.h"
#include "flags.h"
#include "util.h"

uint32_t print_int64     (int64_t   x, uint8_t *pt, flags_w_t flags);
uint32_t print_uint64    (uint64_t  x, uint8_t *pt, flags_w_t flags);

uint32_t print_int128    (int128_t  x, uint8_t *pt, flags_w_t flags);
uint32_t print_uint128   (uint128_t x, uint8_t *pt, flags_w_t flags);

uint32_t print_int64_v     (int64_t   *x, uint8_t *pt, flags_w_t flags);
uint32_t print_uint64_v    (uint64_t  *x, uint8_t *pt, flags_w_t flags);

uint32_t print_int128_v    (int128_t  *x, uint8_t *pt, flags_w_t flags);
uint32_t print_uint128_v   (uint128_t *x, uint8_t *pt, flags_w_t flags);

uint32_t print_int(void *v, uint8_t *pt, flags_w_t flags);

void init_myinteger();
#endif












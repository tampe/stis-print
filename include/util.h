#ifndef MYUTIL_H
#define MYUTIL_H

#define likely(x)        __builtin_expect((x),1)
#define unlikely(x)      __builtin_expect((x),0)

#define REV0

#define mymemset(A,B,C,Sh,D)			\
{						\
  for (int i = 0; i < (C); i+=N)		\
    (A)[(i<<Sh) + D]=(B);			\
}

#define mymemmove(A,B,C,Sh,D)			\
  {						\
    for (int i = (C-1)<<Sh; i >= 0; i--)	\
      (A)[i]=(B)[i];				\
  }

typedef __int128 int128_t;
typedef unsigned __int128 uint128_t;
typedef uint64_t * pos_t;

typedef __float128 float128_t;
typedef double     float64_t;
typedef float      float32_t;
typedef struct PACK float16 {uint16_t x;} float16_t;

int16_t  rev16 (int16_t  x);
int32_t  rev32 (int32_t  x);
int64_t  rev64 (int64_t  x);
int128_t rev128(int128_t x);

uint16_t  urev16 (uint16_t  x);
uint32_t  urev32 (uint32_t  x);
uint64_t  urev64 (uint64_t  x);
uint128_t urev128(uint128_t x);

float16_t  frev16 (float16_t  x);
float32_t  frev32 (float32_t  x);
float64_t  frev64 (float64_t  x);
float128_t frev128(float128_t x);

#endif

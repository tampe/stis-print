#ifndef FLAGS_H
#define FLAGS_H

#include "endian.h"
#include "defs.h"

struct PACK write_flags
{
  uint8_t vecsize     : 8; // Allow a vector of up to 256 elements fixed size
  uint8_t fieldsize   : 8; // size of the field we want to print
  uint8_t prec        : 8; // how many 'digits' of precition for a real
  uint8_t size        : 4; // 8,16,32,64,128 bit sized number or 8,16,32 sized
                           // string
  uint8_t safe_size   : 4; // In number of 2^safe_size bytes, default 7
  uint8_t ch_width    : 2; // How many bytes the character output have
  uint8_t ch1_offset  : 2; // the offset a ASCII have in the output
  uint8_t complex     : 2; // real=0, complex=1, quaternonians=2, octonians=3
  uint8_t convert     : 2; // in utf8 -> latin 1 conversion exception strategies
                           // CONVERT_REPLACE or CONVERT_ESCAPE
  uint8_t base        : 3; // The base used for numbers bases = 2,4,8,16,32
                           // this field is used when binary=1
  uint8_t compiled    : 1; // allows us to check if the flags are compiled
  
  uint8_t endian      : 1; // 0 = native endian, 1 = little endian
                           // 2 = big endian, used for the output characters
  uint8_t real        : 1; // Indicates that a bytevector is real
  uint8_t sign        : 1; // Indicates that a bytevector is signed integers
  uint8_t binary      : 1; // if 1 indicates that we should use the bases above
  uint8_t sep_comma   : 1; // if 1 use comma to separate numbers in bytevectors
                           // and vectors
  uint8_t paren       : 1; // if true use ()
  uint8_t brace       : 1; // id true use {} else []
  uint8_t r_justified : 1; // justify the numbers to the right in the field
  uint8_t c_justified : 1; // justify the numbers to the center in the field
  uint8_t plus        : 1; // use a plus prefix for positive numbers
  uint8_t plus_space  : 1; // use a space prefix for positive numbers
  uint8_t c           : 1; // c style vectors
  uint8_t python      : 1; // python style vectors
  uint8_t scheme      : 1; // scheme style vectors
  uint8_t prefix      : 1; // use a prefix to numbers if binary=1
  uint8_t fillzero    : 1; // pad zeros to the number before it
  uint8_t utf8        : 1; // output strings are utf8
  uint8_t quote       : 1; // if 1 quote strings with ' else "
  uint8_t esc_nl      : 1; // if 1 write \n in stead of a direct newline 
  uint8_t r6rs_esc    : 1; // use scheme r6rs escapes for utf strings
  uint8_t hex_esc     : 1; // if 1 use varying length escapes for strings
  uint8_t write       : 1; // if 1 use the write mode that is either plain
                           // a r7rs scheme version or extended scheme version
  uint8_t r7rs        : 1; // strings will be printed acording to r7rs standard
  uint8_t extended    : 1; // strings will be printed in an extended standard
  uint8_t raw         : 1; // indicates no fancy printing of numbers/integers
                           // with utf8 or latin1 output buffer
};

#define CONVERT_REPLACE 0
#define CONVERT_ESCAPE  1
typedef struct write_flags flags_w_t;

extern flags_w_t default_w_flag;

#endif

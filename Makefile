C    = myreal myinteger mybytevector error util flags
H    = write endian 
H2   = $(C) $(H)
C2   = $(C)

DEPS = $(patsubst %,  include/%.h, $(H2)) Makefile
CODE = $(patsubst %,  src/%.c,     $(C2))

# ----------------------- STR    ----------------------
str: .libs/mystring.o

.libs/mystring.o: src/mystring.c src/util.c src/error.c src/flags.c src/str/latin1-to-latin1.c src/str/latin1-to-utf8.c src/str/utf32-to-x.c src/str/string-meta.c src/str/util.c

	gcc -c -O3 -msse3 -Wall -fcompare-debug-second src/mystring.c
	mv mystring.o .libs/mystring.o

# ----------------------- BV     ----------------------
bv: .libs/mybytevector.o
.libs/mybytevector.o: src/mybytevector.c src/myreal.c src/myinteger.c
	gcc -c -Wall -fcompare-debug-second src/mybytevector.c
	mv mybytevector.o .libs/mybytevector.o

# ----------------------- INTEGER ---------------------
int: .libs/myinteger.o
.libs/myinteger.o : src/myinteger.c src/util.c src/error.c src/flags.c include/myinteger.h include/util.h include/error.h include/flags.h include/endian.h

	gcc -c -O3 -Wall -fcompare-debug-second src/myinteger.c
	mv myinteger.o .libs/myinteger.o

# ----------------------- REAL ---------------------
real: .libs/myreal.o
.libs/myreal.o : src/myinteger.c src/myreal.c src/util.c src/error.c src/flags.c include/myreal.h include/myinteger.h include/util.h include/error.h include/flags.h include/endian.h src/float16.c

	gcc -c -O3 -Wall -fcompare-debug-second src/myreal.c
	mv myreal.o .libs/myreal.o

# ----------------------- TEST (INT) ------------------------
test1: .libs/myinteger.o .libs/test1.o Makefile
	gcc -O3 -o test1 .libs/myinteger.o .libs/test1.o -lm -lquadmath

.libs/test1.o: src/test1.c .libs/myinteger.o Makefile
	gcc -c -fcompare-debug-second -O3 src/test1.c
	mv test1.o .libs/test1.o

# ----------------------- TEST (REAL) ------------------------
test2:	.libs/myreal.o .libs/test2.o
	gcc -o test2 .libs/myreal.o .libs/test2.o -lm -lquadmath

.libs/test2.o: src/test2.c .libs/myreal.o
	gcc -c -fcompare-debug-second -O3 src/test2.c
	mv test2.o .libs/test2.o

# --------------------- DOC ---------------------------

info: doc/serialSpeeder.texi Makefile
	cd doc; makeinfo serialSpeeder.texi

html: doc/serialSpeeder.texi Makefile
	cd doc; makeinfo --html serialSpeeder.texi
